-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-10-2019 a las 22:09:32
-- Versión del servidor: 10.1.35-MariaDB
-- Versión de PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `seminario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `amortizaciones`
--

CREATE TABLE `amortizaciones` (
  `id_amortizacion` int(11) NOT NULL,
  `id_matricula` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT CURRENT_TIMESTAMP,
  `fecha_amortizacion` varchar(255) DEFAULT NULL,
  `nrooperacion` varchar(50) DEFAULT NULL,
  `monto` varchar(255) DEFAULT NULL,
  `estado_registro` enum('Activo','Inactivo') DEFAULT 'Activo',
  `vaucher` varchar(200) NOT NULL,
  `comprobante` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `amortizaciones`
--

INSERT INTO `amortizaciones` (`id_amortizacion`, `id_matricula`, `fecha_registro`, `fecha_amortizacion`, `nrooperacion`, `monto`, `estado_registro`, `vaucher`, `comprobante`) VALUES
(1, 1, '2019-09-26 07:27:57', '03/09/2019', 'qwe', '23', NULL, '', ''),
(2, 2, '2019-10-02 23:20:08', '03/09/2019', 'qwe', '23', NULL, '', ''),
(3, 3, '2019-10-04 13:19:19', '03/09/2019', 'qwe', '23', 'Activo', 'af77d-captura.png', '3ceee-captura.png'),
(4, 4, '2019-10-04 13:27:49', '02/09/2019', 'qwe', '23', 'Activo', 'f0335-captura.png', '4eefb-captura.png'),
(5, 3, '2019-10-04 13:40:51', '02/09/2019', 'qwe', '23', 'Activo', '6c31f-captura.png', '26883-captura.png'),
(6, 5, '2019-10-04 13:42:03', '03/09/2019', 'qwe', '23', 'Activo', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE `asistencia` (
  `id_asistencia` int(11) NOT NULL,
  `fecha_asistencia` datetime DEFAULT CURRENT_TIMESTAMP,
  `id_matricula` int(11) DEFAULT NULL,
  `id_curso` int(11) DEFAULT NULL,
  `estado_registro` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistente`
--

CREATE TABLE `asistente` (
  `id_asistente` int(11) NOT NULL,
  `nombres` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `dni` varchar(8) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `estado` enum('Inactivo','Activo') NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asistente`
--

INSERT INTO `asistente` (`id_asistente`, `nombres`, `apellidos`, `correo`, `dni`, `telefono`, `estado`) VALUES
(1, 'qwe', 'qwe', 'qwe', 'qwe', 'qew', 'Activo'),
(2, 'Colbert', 'Calampa', '73031934', 'correo', '123', 'Inactivo'),
(3, 'Colbert', 'Tantachuco', '73031934', 'colbersi', '973949944', 'Activo'),
(4, 'asd', 'sad', 'qwe', 'qwe', 'qew', ''),
(5, 'contras', 'sad', 'qwe', 'qwe', 'qew', 'Activo'),
(6, 'ñññññ', 'wqe', 'qwe', 'qew', 'qwe', 'Activo'),
(7, 'ísadasaá', 'sda', 'asd', 'sad', 'asd', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `id_curso` int(11) NOT NULL,
  `id_docente` int(11) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  `lugar` varchar(30) NOT NULL,
  `estado` enum('Inactivo','Activo') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id_curso`, `id_docente`, `descripcion`, `lugar`, `estado`) VALUES
(1, 2, 'sd', 'asd', 'Activo'),
(2, 2, 'Leche', 'asd', 'Activo'),
(3, 2, 'perro', 'asd', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso_matricula`
--

CREATE TABLE `curso_matricula` (
  `id_curso_matricula` int(11) NOT NULL,
  `id_curso` int(11) DEFAULT NULL,
  `id_matricula` int(11) DEFAULT NULL,
  `prioridad` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `curso_matricula`
--

INSERT INTO `curso_matricula` (`id_curso_matricula`, `id_curso`, `id_matricula`, `prioridad`, `estado`, `fecha`) VALUES
(1, 2, 1, 0, 1, '2019-10-04 12:50:36'),
(2, 3, 1, 1, 1, '2019-10-04 12:53:13'),
(4, 2, 3, NULL, 1, '2019-10-04 12:54:31'),
(5, 2, 4, NULL, 1, '2019-10-04 13:02:33'),
(6, 3, 4, NULL, 1, '2019-10-04 13:02:34'),
(9, 2, 5, 0, 1, '2019-10-04 13:05:52'),
(10, 3, 5, 1, 1, '2019-10-04 13:05:53'),
(11, 1, 5, 2, 1, '2019-10-04 13:05:53'),
(12, 2, 6, 1, 1, '2019-10-04 14:08:34'),
(13, 3, 6, 2, 1, '2019-10-04 14:08:34'),
(15, 1, 6, 0, 1, '2019-10-04 15:03:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE `docente` (
  `nombres` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `id_docente` int(11) NOT NULL,
  `estado` enum('Activo','Inactivo') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `docente`
--

INSERT INTO `docente` (`nombres`, `apellidos`, `id_docente`, `estado`) VALUES
('asd', 'sad', 1, 'Activo'),
('contras', 'colbert', 2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id_empresa` int(11) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `direcion` varchar(30) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `instragram` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `razon_social` varchar(30) NOT NULL,
  `ruc` varchar(20) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `twiter` varchar(100) NOT NULL,
  `estado` enum('Inactivo','Activo') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

CREATE TABLE `galeria` (
  `id_galeria` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `url` varchar(300) NOT NULL,
  `estado` enum('Inactivo','Activo') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matricula`
--

CREATE TABLE `matricula` (
  `id_matricula` int(11) NOT NULL,
  `costo_matricula` float(8,2) NOT NULL,
  `fecha_registro` date NOT NULL,
  `id_asistente` int(11) NOT NULL,
  `estado_matricula` enum('prematricula','matricula') DEFAULT 'prematricula',
  `estado_registro` enum('Activo','Inactivo') DEFAULT 'Activo',
  `fecha_matricula` date DEFAULT NULL,
  `id_tipo_matricula` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `matricula`
--

INSERT INTO `matricula` (`id_matricula`, `costo_matricula`, `fecha_registro`, `id_asistente`, `estado_matricula`, `estado_registro`, `fecha_matricula`, `id_tipo_matricula`) VALUES
(1, 324.00, '2019-09-05', 2, 'matricula', 'Activo', '2019-10-04', 1),
(2, 324.00, '2019-10-17', 3, 'prematricula', 'Inactivo', NULL, 1),
(3, 324.00, '2019-10-17', 4, 'matricula', 'Activo', NULL, 1),
(4, 324.00, '2019-10-17', 1, 'prematricula', 'Activo', NULL, 1),
(5, 324.00, '2019-10-17', 3, 'matricula', 'Activo', NULL, 1),
(6, 324.00, '2019-10-17', 5, 'matricula', 'Activo', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_matricula`
--

CREATE TABLE `tipo_matricula` (
  `id_tipo_matricula` int(11) NOT NULL,
  `descripcion` int(11) NOT NULL,
  `precio` float(8,2) NOT NULL,
  `estado` enum('Activo','Inactivo') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_matricula`
--

INSERT INTO `tipo_matricula` (`id_tipo_matricula`, `descripcion`, `precio`, `estado`) VALUES
(1, 0, 35.00, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `usuario`, `clave`, `estado`) VALUES
(1, 'admin', 'admin6', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `amortizaciones`
--
ALTER TABLE `amortizaciones`
  ADD PRIMARY KEY (`id_amortizacion`);

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD PRIMARY KEY (`id_asistencia`);

--
-- Indices de la tabla `asistente`
--
ALTER TABLE `asistente`
  ADD PRIMARY KEY (`id_asistente`);

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id_curso`);

--
-- Indices de la tabla `curso_matricula`
--
ALTER TABLE `curso_matricula`
  ADD PRIMARY KEY (`id_curso_matricula`);

--
-- Indices de la tabla `docente`
--
ALTER TABLE `docente`
  ADD PRIMARY KEY (`id_docente`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id_empresa`);

--
-- Indices de la tabla `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id_galeria`);

--
-- Indices de la tabla `matricula`
--
ALTER TABLE `matricula`
  ADD PRIMARY KEY (`id_matricula`);

--
-- Indices de la tabla `tipo_matricula`
--
ALTER TABLE `tipo_matricula`
  ADD PRIMARY KEY (`id_tipo_matricula`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `amortizaciones`
--
ALTER TABLE `amortizaciones`
  MODIFY `id_amortizacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `asistente`
--
ALTER TABLE `asistente`
  MODIFY `id_asistente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `curso_matricula`
--
ALTER TABLE `curso_matricula`
  MODIFY `id_curso_matricula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `docente`
--
ALTER TABLE `docente`
  MODIFY `id_docente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id_empresa` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id_galeria` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `matricula`
--
ALTER TABLE `matricula`
  MODIFY `id_matricula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `tipo_matricula`
--
ALTER TABLE `tipo_matricula`
  MODIFY `id_tipo_matricula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
