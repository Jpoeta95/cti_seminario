<?php 
	
	// $telefono = '955 941 992';
	$unirse_curso=" INVITACIÓN  ";
	$unirse_curso1="LA #UNSM INVITA A PARTICIPAR DEL II #SEMIRARIOINTERNACIONAL TITULADO 'APLICABILIDAD DE LAS TIC´s EN LA GESTIÓN DEL CONOCIMIENTO', ENFOCADO A ESTUDIANTES, PROFESIONALES Y PÚBLICO EN GENERAL CONOCEDORES DE HERRAMIENTAS TECNOLÓGICAS, LOS PONENTES PRESENTARAN CURSOS TALLERES PARA COMPLEMENTAR LAS IDEAS ABORDADAS EN SUS PONENCIAS, CON EL OBJETIVO DE APLICAR LOS CONOCIMIENTOS APRENDIDOS EN EL DESEMPEÑO ACADÉMICO Y/O LABORAL.
		<br> ✍️Inscripciones: En la oficina del CTI - Complejo Universitario.
		<br> 📧 ctiunsm@unsm.edu.pe
		<br> 📞042 -480142 📱944929637 - 955941992.";
	
	$estar_conctacto="Estar en contacto";

	$footer_about_text = "In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum, tempor lacus.";


	
					   
    $header  = array(  array('Menu', array("index.php","Inicio"),array("#","Nosotros"),array("curso.php","Curso"), array("#","Contacto")),
				   array('Usefull Links', array("Testimonials","FAQ","Community","Campus Pictures","Tuitions")),
				   array('Contacto', array("images/placeholder.svg","Blvd Libertad, 34 m05200 Arévalo"),array("images/smartphone.svg","955 941 992"),array("images/envelope.svg","hello@company.com"))
			   );
?>


<?php 

	include('config.php');

	include('cabecera.php');

?>

<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/contact_styles.css">
<link rel="stylesheet" type="text/css" href="styles/contact_responsive.css">

	<!-- Home -->

	<div class="home">
		<div class="home_background_container prlx_parent">
			<div class="home_background prlx" style="background-image:url(<?= $imagenes_web['fondo']['contacto'] ?>)"></div>
		</div>
		<div class="home_content">
			<h1>Contacto</h1>
		</div>
	</div>

	<!-- Contact -->

	<div class="contact">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					
					<!-- Contact Form -->
					<div class="contact_form">
						<div class="contact_title"><?= $estar_conctacto ?></div>

						<div class="contact_form_container">
							<form action="post">
								<input id="contact_form_name" class="input_field contact_form_name" type="text" placeholder="Nombre" required="required" data-error="Name is required.">
								<input id="contact_form_email" class="input_field contact_form_email" type="email" placeholder="Correo" required="required" data-error="Valid email is required.">
								<textarea id="contact_form_message" class="text_field contact_form_message" name="message" placeholder="Mensaje" required="required" data-error="Please, write us a message."></textarea>
								<button id="contact_send_btn" type="button" class="contact_send_btn trans_200" value="Submit">Enviar Mensaje</button>
							</form>
						</div>
					</div>
						
				</div>

				<div class="col-lg-5">
					<div class="about">
						<div class="about_title"><?=$unirse_curso?></div>
						<p class="about_text"><?=$unirse_curso1?></p>

						<!--div class="contact_info">
							<ul>
							<? foreach ($informacion_contacto['direccion'] as $key => $value) : ?>
								<li class="footer_list_item">
									<div class="footer_contact_icon">
										<img src="images/placeholder.svg" alt="https://www.flaticon.com/authors/lucy-g">
									</div>									
									<?= $value ?>
								</li>
								<? endforeach; ?>

								<? foreach ($informacion_contacto['telefono'] as $key => $value) : ?>
								<li class="footer_list_item">
									<div class="footer_contact_icon">
										<img src="images/smartphone.svg" alt="https://www.flaticon.com/authors/lucy-g">
									</div>									
									<?= $value ?>
								</li>
								<? endforeach; ?>

								<? foreach ($informacion_contacto['correo'] as $key => $value) : ?>
								<li class="footer_list_item">
									<div class="footer_contact_icon">
										<img src="images/envelope.svg" alt="https://www.flaticon.com/authors/lucy-g">
									</div>									
									<?= $value ?>
								</li>
								<? endforeach; ?>
							</ul>
						</div-->

					</div>
				</div>

			</div>

			<!-- Google Map -->

			<div class="row">
				<div class="col">
					<div id="google_map">
						<div class="map_container">
							<div  >
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.266487241387!2d-76.36898438585966!3d-6.487900065235754!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91ba0bf9d6724f4d%3A0x561e59184d9b7c68!2sJr.%20Orellana%2C%20Tarapoto%2022201!5e0!3m2!1ses!2spe!4v1567676506357!5m2!1ses!2spe" 
								width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="">
								</iframe>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

<?php 

	include('pie.php');

?>
	
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
