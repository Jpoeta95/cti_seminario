<?php 
	include('config.php');
	$evento = 'II Semininario Internacional de Tecnología' ;
	// $telefono = '955 941 992';
	$unirse_curso="Unirse a cursos";
	$unirse_curso1="In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum. Etiam eu purus nec eros varius luctus. Praesent finibus risus facilisis ultricies. Etiam eu purus nec eros varius luctus.";
	
	$estar_conctacto="Estar en contacto";

	$logo_sm = 'images/logo.png';
	$logo_xs = 'images/logo.png';

	$direccion = 'Jr. Orellana 575 - Tarapoto';
	$correo = '@cti';
	$footer_about_text = "In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum, tempor lacus.";
	
	$sql = "SELECT CONCAT(nombres, ' ', apellidos)as nombre, resumen,cargo,img, id_docente FROM docente WHERE estado='Activo'";
	$result = $conexion->prepare($sql);
	$result->execute(array());   
	$ponente = $result->fetchAll();

	$modulos_web  = array(  array('Inicio', 'index.php'),
						array('Nosotros', 'nosotros.php'),
						array('Cursos', 'cursos.php'),
						array('Ponentes', 'ponentes.php'),
						array('Noticias', 'noticias.php'),
						array('Contacto', 'contacto.php')
					);
	$menu_social  = array(  'fab fa-pinterest','fab fa-linkedin-in',
						'fab fa-instagram',	'fab fa-facebook-f','fab fa-twitter'
					   );
					   
    $header  = array(  array('Menu', array("index.php","Inicio"),array("#","Nosotros"),array("curso.php","Curso"), array("#","Contacto")),
				   array('Usefull Links', array("Testimonials","FAQ","Community","Campus Pictures","Tuitions")),
				   array('Contacto', array("images/placeholder.svg","Blvd Libertad, 34 m05200 Arévalo"),array("images/smartphone.svg","955 941 992"),array("images/envelope.svg","hello@company.com"))
			   );

	// $ponente  = array(  array('img'=>'images/teacher_1.jpg','nombre'=>'Maria Smith','cargo'=>'Graphic Designer', array(  'fab fa-pinterest','fab fa-linkedin-in','fab fa-instagram','fab fa-facebook-f','fab fa-twitter')),
	// 		            array('img'=>'images/teacher_2.jpg','nombre'=>'Christian Blue','cargo'=>'Graphic Designer', array(  'fab fa-pinterest','fab fa-linkedin-in','fab fa-instagram','fab fa-facebook-f','fab fa-twitter')),
	// 	                array('img'=>'images/teacher_3.jpg','nombre'=>'James Brown','cargo'=>'Graphic Designer', array(  'fab fa-pinterest','fab fa-linkedin-in','fab fa-instagram','fab fa-facebook-f','fab fa-twitter'))
	// 			);
	$canti_mat  = array(  array('img'=>'images/milestone_1.svg','cantidad'=>'750','cant_per'=>'Current Students'),
	                      array('img'=>'images/milestone_2.svg','cantidad'=>'120','cant_per'=>'Certified Teachers'),
		                 array('img'=>'images/milestone_3.svg','cantidad'=>'39','cant_per'=>'Approved Courses'),
                         array('img'=>'images/milestone_4.svg','cantidad'=>'3500','cant_per'=>'Graduate Students')
		   );
	$sliders_web  = array(  array('img' => 'images/slider_background.jpg',
								  'texto' => "¡Obtenga <span>su reservación</span> hoy!")
					);
	$bienvenida_profe  = array( 'nombre_bienvenida'=>'Become a teacher', 'contenido_bienvenida'=>'In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum. Etiam eu purus nec eros varius luctus. Praesent finibus risus facilisis ultricies venenatis. Suspendisse fermentum sodales lacus, lacinia gravida elit dapibus sed. Cras in lectus elit. Maecenas tempus nunc vitae mi egestas venenatis. Aliquam rhoncus, purus in vehicula porttitor, lacus ante consequat purus, id elementum enim purus nec enim. In sed odio rhoncus, tristique ipsum id, pharetra neque.', 
	                            'boton_bienvenida'=>'Read More','img_bienvenida'=>'images/become.jpg');

?>


<?php 

	include('config.php');

	include('cabecera.php');

?>

<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/teachers_styles.css">
<link rel="stylesheet" type="text/css" href="styles/news_styles.css">
<link rel="stylesheet" type="text/css" href="styles/teachers_responsive.css">
	
	<!-- Home -->

	<div class="home">
		<div class="home_background_container prlx_parent">
			<div class="home_background prlx" style="background-image:url(images/teachers_background.jpg)"></div>
		</div>
		<div class="home_content">
			<h1>Ponentes</h1>
		</div>
	</div>

	<!-- Teachers -->

	<div class="teachers page_section">
		<div class="container">
			<div class="row">
				
				<!-- Teacher -->
				<? foreach ($ponente as $key => $val) : ?>
					<div class="col-lg-4 teacher">
						<div class="card">
							<div class="card_img">
								<div class="card_plus trans_200 text-center"><a onclick="enviar('<?=utf8_encode($val['nombre'])?>','<?=utf8_encode($val['cargo'])?>','<?=$val['id_docente']?>')">+</a></div>
								<img class="card-img-top trans_200" src="<?=$val['img']?>" alt="https://unsplash.com/@michaeldam">
							</div>
							<div class="card-body-ponentes text-center">
								<div class="card-title"><a><?=utf8_encode($val['nombre'])?></a></div>
								<div class="card-text"><?=$val['cargo']?></div>
								<!-- <div class="teacher_social">
									<ul class="menu_social">
										<? for ($t=0; $t < count($val) ; $t++): ?>
											<li class="menu_social_item"><a href="#"><i class="<?=$val[0][$t]?>"></i></a></li>
										<? endfor ?>
									</ul>
								</div> -->
							</div>
						</div>
					</div>
				<? endforeach ?>

			</div>
		</div>
	</div>

	<!-- Milestones -->

	<!--div class="milestones">
		<div class="milestones_background" style="background-image:url(images/milestones_background.jpg)"></div>

		<div class="container">
			<div class="row">
				
				
				<? foreach ($canti_mat as $key => $val) : ?>
					<div class="col-lg-3 milestone_col">
						<div class="milestone text-center">
							<div class="milestone_icon"><img src="<?=$val['img']?>" alt="https://www.flaticon.com/authors/zlatko-najdenovski"></div>
							<div class="milestone_counter" data-end-value="<?=$val['cantidad']?>">0</div>
							<div class="milestone_text"><?=$val['cant_per']?></div>
						</div>
					</div>
				<? endforeach ?>
			</div>
		</div>
	</div-->

	<!-- Become -->

	<!--div class="become">
		<div class="container">
			<div class="row row-eq-height">

				<div class="col-lg-6 order-2 order-lg-1">
					<div class="become_title">
						<h1><?=$bienvenida_profe['nombre_bienvenida']?></h1>
					</div>
					<p class="become_text"><?=$bienvenida_profe['contenido_bienvenida']?></p>
					<div class="become_button text-center trans_200">
						<a href="#"><?=$bienvenida_profe['boton_bienvenida']?></a>
					</div>
				</div>

				<div class="col-lg-6 order-1 order-lg-2">
					<div class="become_image">
						<img src="<?=$bienvenida_profe['img_bienvenida']?>" alt="">
					</div>
				</div>
				
			</div>
		</div>
	</div-->

	<div class="modal" id="myModal" >
		<div class="modal-dialog" >
			<div class="modal-content modalm" style="">

			<div class="modal-header" style="align-items: center">
				<h4 class="modal-title" >Modal Heading</h4>
				<button type="button" class="close" data-dismiss="modal" >&times;</button>
			</div>

			<div class="modal-body" style="text-align: justify;">
				Modal body..
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
			</div>

			</div>
		</div>
	</div>

	<script type="text/javascript">

// function activa(){

// 	acc = $('.accordion');//$('#forma_pago');
// 	acc.toggleClass('active');			
// 	var panel = $(acc.next());
// 	var panelH = panel.prop('scrollHeight') + "px";		
// 	if(panel.css('max-height') == "0px")
// 	{	panel.css('max-height', panel.prop('scrollHeight') + "px");
// 	}
// 	else
// 	{	panel.css('max-height', "0px");			
// 	} 

// }

// function curso_secundarios(){
// 	cp = $('#curso_principal');
// 	cs = $('#curso_secundario');
// 	cs.val('');
// 	$("#curso_secundario > option").show();
// 	$("#curso_secundario > option[value=" + cp.val() + "]").hide();
// }

function enviar(nombre,cargo,id){
	myModal = $('#myModal'); 
	parameto = { id_prosesar:id};
	$.post( "ajax/ponentes.php",parameto, function(data) {
     console.log(data);
           myModal.modal('show');
		   myModal.find('.modal-title').html(cargo+"."+nombre);
		   myModal.find('.modal-body').html(data);
		   myModal.find('.modal-footer').hide();
});
	/*$.ajax({
		url: 'ajax/ponentes.php',
		type:'POST',
		data: parameto, 
		dataType: 'json',
		beforeSend: function( xhr ) {
		   myModal.modal('show');
		   myModal.find('.modal-title').html(cargo+"."+nombre);
		   myModal.find('.modal-body').html(id);
		   myModal.find('.modal-footer').hide();
		},
	}).done(function(datos) {
		alert("dkaskdak -------s");
		 console.log(datos);
		      myModal.find('.modal-title').html(cargo+"."+nombre);
			   myModal.find('.modal-body').html(datos.mensaje);

			   myModal.find('.modal-footer').hide();
		});
		alert("dkaskdaks");*/
	// $.ajax({

	// 	 url:'ajax/ponentes.php',
	// 	 type:'POST',
	// 	 data: parameto, 
	// 	 dataType: 'json',
	// 	 beforeSend: function( xhr ) {
	// 	   myModal.modal('show');
	// 	   myModal.find('.modal-title').html(cargo+"."+nombre);
	// 	   myModal.find('.modal-body').html(id);
	// 	   myModal.find('.modal-footer').hide();
	// 	},
	// 	success:function(datos){
    //     console.log(datos);
    //       alert("jajajaj");
	// 		myModal.find('.modal-title').html(cargo+"."+nombre);
	// 		   myModal.find('.modal-body').html(datos.mensaje);

	// 		   myModal.find('.modal-footer').show();
	// 		//    myModal.find('.modal-footer > button').html('Aceptar');

	// 		// if(datos.estado == 1){										
	// 		// 	$('#submit').addClass('send_submit').html('SOLICITUD ENVIADA').prop( "disabled", true );
	// 		// }
	// 	}


	// })

	// return false; 
}



// activa();

</script>


<?php 

  include('pie.php');

?>