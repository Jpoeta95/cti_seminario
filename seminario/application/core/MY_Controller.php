<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    //constructor, debe llamar al constructor de la clase parent

    public $nombre_empresa = 'CTI';
    public $controller = '';
    public $metodo = '';
    public $menu = '';
    
    public $nombre_usuario = '';
    public $perfil_usuario = '';

    public function __construct()
    {
        parent::__construct();        
        
        
    }

    public function _init( $cargar_menu, $cargar_url, $cargar_template )
    {       
    	if( !$this->session->userdata('logeado_sis') ){        
            redirect('login', 'location’');   
        }

        $this->nombre_usuario  = $this->session->userdata('username');

        if($cargar_menu) {$this->menu = $this->_get_menu();} //obtener menu para el usuario en sesion
        if($cargar_url) {$this->_request_url();}//obtengo el pedio URL y reemplaza el controlador y metodo    
        if($cargar_template) {$this->output->set_template('adminlte');}//Carga el template
           
    }

    private function _request_url()
    {
        $url = $this->input->server('REQUEST_URI'); 
        $url_solicitados = explode("/", $url);
        $this->controller =  isset($url_solicitados[2])?ucwords($url_solicitados[2]):'';
        $this->metodo =  isset($url_solicitados[3])?ucwords($url_solicitados[3]):'';  
    } 

    private function _get_menu()
    {        
        
                
        
 
        $modulo_papa_flag = '';
       
        $menu = '
                <li class="treeview">
                <a href="#">
                    <i class="fa fa-reorder"></i> <span> Administrador </span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                  <ul class="treeview-menu">
                    <li><a href="'. base_url() .'cursos/lista"><i class="fa fa-key"></i> Cursos</a></li>
                    <li><a href="'. base_url() .'asistente/lista"><i class="fa fa-tags"></i> Asistente</a></li>
                    <li><a href="'. base_url() .'docente/lista"><i class="fa fa-tags"></i> Docente</a></li>
                    <li><a href="'. base_url() .'tipomatricula/lista"><i class="fa fa-tags"></i> Tipomatricula</a></li>
                    <li><a href="'. base_url() .'empresa/lista"><i class="fa fa-tags"></i> Empresa</a></li>
                    <li><a href="'. base_url() .'galeria/lista"><i class="fa fa-tags"></i> Galeria</a></li>
                    </ul>
                </li>
                <li class="treeview">
                <a href="#">
                    <i class="fa fa-reorder"></i> <span> Matricula </span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                  <ul class="treeview-menu">
                    <li><a href="'. base_url() .'inscritos/lista"><i class="fa fa-key"></i> Inscritos</a></li>
                    <li><a href="'. base_url() .'matricula/lista"><i class="fa fa-tags"></i> Matriculados</a></li>
                    <li><a href="'. base_url() .'cobros/lista"><i class="fa fa-tags"></i> cobros</a></li>
                    
                    
                    </ul>
                </li>
                <li class="treeview">
                <a href="#">
                    <i class="fa fa-reorder"></i> <span> Reporte </span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                  <ul class="treeview-menu">
                    <li><a href="'. base_url() .'Reporte/lista"><i class="fa fa-key"></i> Reporte Digital</a></li>
                    </ul>
                </li>
                '
                ;


        

        return $menu;
        // exit();
    } 

    

}