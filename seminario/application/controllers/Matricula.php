<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Matricula extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->controller = 'Matricula';//Siempre define las migagas de pan
        $this->load->library('grocery_CRUD');
    }


    public function lista()
    {
              $primary_key;
        $this->metodo = 'Lista';//Siempre define las migagas de pan
        $this->load->js('assets/javascrit/prueba.js');

        $crud = new grocery_CRUD();

     
        $crud->set_theme('tablestrap');

     

        $crud->where('estado_matricula =','matricula');
        $crud->set_table('matricula');
        $state_info = $crud->getStateInfo();
        if ($crud->getState() == 'edit' ){

            $primary_key = $state_info->primary_key;
            
            $crud->set_relation('id_asistente','asistente','dni',"id_asistente NOT IN(SELECT matricula.id_asistente from asistente inner join matricula on asistente.id_asistente=matricula.id_asistente where matricula.estado_registro='Activo'and matricula.id_matricula <>".$primary_key." )");
            $crud->display_as('id_asistente','DNI');

        }else{ 
            
            $crud->set_relation('id_asistente','asistente','{nombres} {apellidos}',"id_asistente NOT IN(SELECT matricula.id_asistente from asistente inner join matricula on asistente.id_asistente=matricula.id_asistente )");
            $crud->display_as('id_asistente','Participante');
        }
        
        $crud->set_relation_n_n('detalle', 'curso_matricula', 'cursos', 'id_matricula', 'id_curso', 'descripcion','prioridad',"estado='Activo' and (SELECT COUNT(*) FROM curso_matricula as cm WHERE cm.id_curso = id_curso) < 25");
        $crud->columns('id_asistente','dni','fecha_registro','costo_matricula','estado_matricula','id_tipo_matricula','estado_registro');
        $crud->fields('id_asistente','Asistente','costo_matricula','fecha_registro','estado_matricula','id_tipo_matricula','detalle');
          $crud->edit_fields(array('id_asistente','Asistente','costo_matricula','fecha_registro','id_tipo_matricula','detalle','estado_registro'));
          $crud->where('estado_registro','activo');
        
        $crud->field_type('Asistente', 'readonly');
        $crud->display_as('id_tipo_matricula','Tipo');
        $crud->set_relation('id_tipo_matricula','tipo_matricula','descripcion');
        $crud->callback_column('dni',array($this,'getNombresClientes'));
        $crud->unset_delete();
        $crud->unset_add();
        $crud->callback_after_update(array($this, 'EstadoCambio'));
        $output = $crud->render();
        $output->title = ' Inscritos';


        $this->_init(true,true,true);//Carga el tema ( $cargar_menu, $cargar_url, $cargar_template )
        $this->load->view('grocery_crud/basic_crud', (array)$output ) ;
    }

    function EstadoCambio($post_array,$primary_key){
        if ($post_array['estado_registro']=='Inactivo') {
            $this->db->where('id_matricula', $primary_key);
            $this->db->delete('curso_matricula');
         
        }
        
        return true;

    }

    function getNombresClientes($primary_key,$row) {

        $sql = "SELECT dni
                FROM asistente
                WHERE id_asistente
                IN (
                SELECT id_asistente
                FROM matricula
                where matricula.id_matricula=$row->id_matricula)";
        $result = $this->db->query($sql)->row();
        $title = $result->dni;
                
        return $title;
    }
	

}