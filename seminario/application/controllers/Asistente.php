<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asistente extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->controller = 'Asistente';//Siempre define las migagas de pan
        $this->load->library('grocery_CRUD');
    }


    public function lista()
    {
        //$this->load->js('assets/javascrit/prueba.js');
        $this->metodo = 'Lista';//Siempre define las migagas de pan

        $crud = new grocery_CRUD();
      $crud->set_theme('tablestrap');
        $crud->set_table('asistente');
        
        $crud->unset_delete();
        $output = $crud->render();
        $output->title = 'Asistente';

        $this->_init(true,true,true);//Carga el tema ( $cargar_menu, $cargar_url, $cargar_template )
        $this->load->view('grocery_crud/basic_crud', (array)$output ) ;
    }



	

}