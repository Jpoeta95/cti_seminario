<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporte extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->controller = 'Reporte';//Siempre define las migagas de pan
        //$this->load->views('reporte/index.php');
    }

    public function lista(){
        $output = new stdClass;
        $this->_init(true,true,true);//Carga el tema ( $cargar_menu, $cargar_url, $cargar_template )
        $output->title = 'Reporte en Excel';
        $this->db->select("*");
        $this->db->from("tipo_matricula");
        $this->db->where('estado','Activo');
        $r = $this->db->get();
        $results = $r->result_array();
        // print_r($results);
        // exit;
        $output->resultado = $results;
        $this->load->view('reporte/index', (array)$output );
    }

    public function cargar_reporte($modalidad){
        
        $output = new stdClass;
        if ($modalidad!='todo') {
            $this->db->select("descripcion");
            $this->db->from("tipo_matricula");
            $this->db->where('estado','Activo');
            $this->db->where('id_tipo_matricula',$modalidad);
            $r = $this->db->get();
            $results = $r->result_array();
            $output->title = $results;
        }else {
            $output->title = array(0 => array('descripcion'=>'todas las modalidades'));
        }
        
        // print_r($output);
        // exit;
        
        if($modalidad!='todo'){
            $this->db->select("a.dni as dni,CONCAT(a.apellidos,', ',a.nombres) As nombres,t.descripcion as 	descripcion, SUM(amo.monto) as monto, m.costo_matricula as costo");
            $this->db->from("matricula as m");
            $this->db->join("asistente as a","m.id_asistente=a.id_asistente");
            $this->db->join("tipo_matricula as t","t.id_tipo_matricula=m.id_tipo_matricula");
            $this->db->join("amortizaciones as amo","amo.id_matricula = m.id_matricula");
            $this->db->where('m.estado_registro','Activo');
            $this->db->where('m.id_tipo_matricula',$modalidad);
            $this->db->order_by("m.id_matricula", "asc");
            $r = $this->db->get();

        }else {
            $this->db->select("a.dni as dni,CONCAT(a.apellidos,', ',a.nombres) As nombres,t.descripcion as 	descripcion, SUM(amo.monto) as monto, m.costo_matricula as costo");
            $this->db->from("matricula as m");
            $this->db->join("asistente as a","m.id_asistente=a.id_asistente");
            $this->db->join("tipo_matricula as t","t.id_tipo_matricula=m.id_tipo_matricula");
            $this->db->join("amortizaciones as amo","amo.id_matricula = m.id_matricula");
            $this->db->where('m.estado_registro','Activo');
            $this->db->order_by("m.id_matricula", "asc");
            $r = $this->db->get();
        }
        
        $results = $r->result_array();
        // print_r($results);
        // exit;
        $output->resultado = $results;
        // $output->title = $modalidad;
        $this->load->view('reporte/excel', (array)$output );
    }
}