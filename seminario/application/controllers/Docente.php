<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Docente extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->controller = 'Docente';//Siempre define las migagas de pan
        $this->load->library('grocery_CRUD');
    }


    public function lista()
    {
        $this->metodo = 'Lista';//Siempre define las migagas de pan

        $crud = new grocery_CRUD();
        $crud->set_theme('tablestrap');
        $crud->set_table('docente');
        
        $crud->unset_delete();
        $output = $crud->render();
        $output->title = 'Docente';

        $this->_init(true,true,true);
        $this->load->view('grocery_crud/basic_crud', (array)$output ) ;
    }


	

}