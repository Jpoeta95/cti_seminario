yt<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeria extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->controller = 'Galeria';//Siempre define las migagas de pan
        $this->load->library('grocery_CRUD');
    }


    public function lista()
    {
        $this->metodo = 'Lista';//Siempre define las migagas de pan

        $crud = new grocery_CRUD();
        $crud->set_theme('tablestrap');
        
        $crud->set_table('galeria');
        $crud->set_field_upload('url','assets/uploads/files');
        $crud->display_as('id_empresa','Empresa');
        $crud->set_relation('id_empresa','empresa','razon_social');  
        $crud->unset_delete();
        $output = $crud->render();
        $output->title = 'Galeria';

        $this->_init(true,true,true);//Carga el tema ( $cargar_menu, $cargar_url, $cargar_template )
        $this->load->view('grocery_crud/basic_crud', (array)$output ) ;
    }


	

}