<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inscritos extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->controller = 'Inscritos';//Siempre define las migagas de pan
        $this->load->library('grocery_CRUD');
    }


    public function lista()
    {
        $primary_key;
        $this->metodo = 'Lista';//Siempre define las migagas de pan
        $this->load->js('assets/javascrit/prueba.js');

        $crud = new grocery_CRUD();

     
        $crud->set_theme('tablestrap');

     
        $crud->where('estado_matricula =','prematricula');
        $crud->set_table('matricula');
        $state_info = $crud->getStateInfo();
        //die($crud->getState());
        if ($crud->getState() == 'edit' or $crud->getState()== 'read' )
        {

            $primary_key = $state_info->primary_key;            
            $crud->set_relation('id_asistente','asistente','dni',"id_asistente NOT IN(SELECT matricula.id_asistente from asistente inner join matricula on asistente.id_asistente=matricula.id_asistente where  matricula.id_matricula <>".$primary_key." )");

            $crud->display_as('id_asistente','DNI');

        }else{ 
            if ($crud->getState() == 'add' ){
                 $crud->set_relation('id_asistente','asistente','dni',"id_asistente NOT IN(SELECT matricula.id_asistente from asistente inner join matricula on asistente.id_asistente=matricula.id_asistente where estado_registro='Activo')");

                $crud->display_as('id_asistente','DNI');

            }else{

                $crud->set_relation('id_asistente','asistente','{nombres} {apellidos}',"id_asistente NOT IN(SELECT matricula.id_asistente from asistente inner join matricula on asistente.id_asistente=matricula.id_asistente )");

                $crud->display_as('id_asistente','Participante');
            }   
            
        }
        
        
        $crud->set_relation_n_n('detalle', 'curso_matricula', 'cursos', 'id_matricula', 'id_curso', 'descripcion','prioridad',"estado='Activo' and (SELECT COUNT(*) FROM curso_matricula as cm WHERE cm.id_curso = id_curso) < 25");
        $crud->set_relation('id_tipo_matricula','tipo_matricula','descripcion');
        
        $crud->columns('id_asistente','dni','fecha_registro','costo_matricula',/*'estado_matricula',*/'id_tipo_matricula','estado_registro');
        $crud->fields('id_asistente','Asistente','costo_matricula','fecha_registro','estado_matricula','id_tipo_matricula','detalle');
        $crud->set_read_fields('id_asistente','Asistente','costo_matricula','fecha_registro','estado_matricula','id_tipo_matricula','estado_registro');
        $crud->edit_fields(array('id_asistente','Asistente','costo_matricula','fecha_registro','estado_matricula','id_tipo_matricula','detalle','estado_registro'));
        
        $crud->field_type('Asistente', 'readonly');
        $crud->display_as('id_tipo_matricula','Tipo');
         $crud->callback_read_field('Asistente', function ($value, $primary_key) {

         $sql = "SELECT concat_ws(' ', s.nombres, s.apellidos) as nombres
                FROM asistente as s inner join matricula as m  on s.id_asistente=m.id_asistente
                where m.id_matricula=$primary_key";
                $result = $this->db->query($sql)->row();
            $title = $result->nombres;
            return $title;
        });
        $crud->callback_column('dni',array($this,'getNombresClientes'));
        $crud->unset_delete();
        $crud->callback_after_update(array($this, 'EstadoCambio'));
        $output = $crud->render();
        $output->title = ' Inscritos';


        $this->_init(true,true,true);//Carga el tema ( $cargar_menu, $cargar_url, $cargar_template )
        $this->load->view('grocery_crud/basic_crud', (array)$output ) ;
    }


function EstadoCambio($post_array,$primary_key){
    date_default_timezone_set('America/Los_Angeles');
    $fecha_actual=date("Y/m/d");
    if ($post_array['estado_registro']=='Inactivo') {
        $this->db->where('id_matricula', $primary_key);
        $this->db->delete('curso_matricula');     
    }

    if ($post_array['estado_matricula']=='matricula') {
        $this->db->set('fecha_matricula', $fecha_actual);
        $this->db->where('id_matricula', $primary_key);
        $this->db->update('matricula');     
    }
    
    return true;
}
      
    function getNombresClientes($primary_key,$row) {

            $sql = "SELECT dni
                    FROM asistente
                    WHERE id_asistente
                    IN (
                    SELECT id_asistente
                    FROM matricula
                    where matricula.id_matricula=$row->id_matricula)";
            $result = $this->db->query($sql)->row();
            $title = $result->dni;
                    
            return $title;
    }
	
    function nombresasistentes(){

        $id=$this->input->post("holi");
         $sql = "SELECT concat_ws(' ', nombres, apellidos) as nombres
                FROM asistente
                where id_asistente=$id";
                $result = $this->db->query($sql)->row();

                echo json_encode($result);

    }
      function nombresasistentes2(){

        $id=$this->input->post("holi");
         $sql = "SELECT concat_ws(' ', s.nombres, s.apellidos) as nombres
                FROM asistente as s inner join matricula as m  on s.id_asistente=m.id_asistente
                where m.id_matricula=$id";
                $result = $this->db->query($sql)->row();

                echo json_encode($result);

    }

}