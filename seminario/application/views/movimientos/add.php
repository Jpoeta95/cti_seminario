<style type="text/css">
  #div_detalle_venta{
    max-height: 350px;
    overflow-y: scroll;  
  }

  td.prod {
    width: 50% !important;  
  }
  td.cant {
    width: 10% !important;  
  }
  td.prec, td.sub {
    width: 20% !important;  
  }
  td > input {
    width: 100% !important;  
  }

  .select2-container{
    height: auto !important;
  }

  body{
    padding: 0px !important;
  }


</style>


<div class="row" id="form-venta">
  <!-- left column -->
  <div class="col-md-8">
    <div class="box box-success">
      <div class="box-header with-border">
        <div class="input-group input-lg">
          <!--span class="input-group-addon">
            <input type="checkbox" checked="checked" id="tipo_busqueda">
          </span-->
          <input class="form-control input-lg" id="input_buscar_codigo" type="text" placeholder="Buscar codigo barra..." autofocus="autofocus" tabindex="1">
          <select class="form-control select2" id="input_buscar_producto" tabindex="-1">
            <option></option>
          </select>  
          <span class="input-group-addon">
            <button id="get_lista_productos" type="button" class="btn btn-info btn-flat" onclick="get_lista_productos()" tabindex="-1"><i class="fa fa-refresh"></i></button>
          </span>
        </div>
      </div>
      <div class="box-body">

        <div id="div_detalle_venta">
          <table class="table table-striped" id="table_detalle_venta">
            
            <tbody>   
              <!--DETALLE DE VENTA-->
              
            </tbody>
          </table>
        </div>  
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="row">

          <div class="col-xs-7">
            <p># Articulos : <span id="cantidadItem">0</span></p>
          </div>
          <div class="col-xs-5 ">
            <!--h3 class="box-title" id="text_total_venta" > TOTAL S/. <span id="total_venta">0.00</span></h3-->
            <div class="form-group">
                <h3>TOTAL S/. <span id="total_venta">0.00</span> </h3>
            
            </div>
          </div>  
          
        </div>
        
        <p><code>F9: Guardar movimiento</code></p>
        <p><code>Ctrl + B: Busqueda rápida</code></p>
      </div>

    </div>
  </div>

  <div class="col-md-4">
    <div class="box box-info">     

      <form class="form-horizontal">
        <div class="box-body">
          <div class="form-group">
         
              <label for="fecha_venta" class="control-label col-sm-2">FECHA:</label>
         
            <div class="col-sm-10">
              <input type="date" class="form-control" id="fecha_venta" name="fecha_venta" value="<?php echo date('Y-m-d');?>" readonly="readonly">
              
            </div>
          </div>
            
          <div class="form-group">
           
              <label for="tienda" class="control-label col-sm-2">TIENDA:</label>  
            
            <div class="col-sm-10">
              
              <select class="form-control" id="tienda" name="tienda">
           
                <?php foreach ($tiendas as $tienda): ?>
                <option value="<?= $tienda->idtienda ?>"><?= $tienda->descripcion ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
            
            <div class="form-group">
           
              <label for="tienda" class="control-label col-sm-2">TIPO:</label>  
            
            <div class="col-sm-10">
                <select class="form-control" id="tipomovimiento" name="tipomovimiento" onchange="get_correlativo()">
                      <option value="Ingreso">Ingreso</option>
                      <option value="Salida">Salida</option>
                </select>
            </div>
          </div>

          <div class="form-group">
            <label for="idserie" class="col-sm-2 control-label">NRO:</label>
            
            <div class="col-sm-10">
                <input type="text" class="form-control" id="nrocomprobante" readonly name="nrocomprobante" value="" value="">
              
            </div>
          </div>
            
           <div class="form-group">
            <label for="idserie" class="col-sm-2 control-label">TOTAL</label>
            
            <div class="col-sm-10">
              <input type="text" class="form-control" id="totalmovimiento" readonly name="totalmovimiento" value="0.00" value="">
              
            </div>
          </div>
        

         

          <textarea class="form-control" rows="2" placeholder="Observaciones ..." id="observacion" name="observacion"></textarea>
          

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <a type="button" class="btn btn-danger" href="<?php echo base_url('movimientos/lista'); ?>">Cancelar</a>
          <button type="button" class="btn btn-success pull-right" id="btn_save" onclick="save()">Guardar</button>

        </div>
        <!-- /.box-footer -->
      </form>
    </div>

  </div>  

</div>

<script type="text/javascript"> base_url = "<?php echo base_url();  ?>"</script>
