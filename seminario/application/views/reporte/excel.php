<?php
	header("Content-type: application/vnd.ms-excel; name='excel'");
	header("Content-Disposition: filename=Reportes_Modalidad.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
?>
<table class="ui-widget" border="1" cellspacing="1" id="TbTitulo" width="50%" rules="rows">
    <thead class="ui-widget-header" style="font-size:14px">
        <tr title="Cabecera">
            <th scope="col" colspan="6" align="center" >REPORTES DE: <?= strtoupper($title[0]['descripcion']); ?></th>
        </tr>
        <!-- <tr>
            <th colspan="14">PERIODO:</th>
        </tr> -->
    </thead>
    <tbody style="font-size:12px">
        <tr >
	        <th width="220" scope="col" class="ui-widget-header">Item</th>
	        <th width="220" scope="col" class="ui-widget-header">DNI</th>
	        <th width="220" scope="col" class="ui-widget-header">NOMBRE</th>
	        <th width="220" scope="col" class="ui-widget-header">MODALIDAD</th>
	        <th width="220" scope="col" class="ui-widget-header">COSTO MATRICULA</th>
	        <th width="220" scope="col" class="ui-widget-header">ESTADO</th>
      </tr>
	  <? foreach ($resultado as $key => $val) : ?>
	  <tr >
		<th width="220" scope="col" class="ui-widget-header"><?php if($val['monto']!=null){echo $key;} ?></th>
		<th width="220" scope="col" class="ui-widget-header"><?= $val['dni']?></th>
		<th width="220" scope="col" class="ui-widget-header"><?= $val['nombres']?></th>
		<th width="220" scope="col" class="ui-widget-header"><?= $val['descripcion']?></th>
		<th width="220" scope="col" class="ui-widget-header"><?= $val['costo']?></th>
		<th width="220" scope="col" class="ui-widget-header"><?php if($val['monto']==$val['costo']){ if($val['monto']!=null){echo "CANCELADO";}}else{ $RESULTADO = $val['costo']-$val['monto'];echo "DEBE S/$RESULTADO";}?></th>
	 </tr>
	 <? endforeach; ?>
  </tbody>
</table>
		