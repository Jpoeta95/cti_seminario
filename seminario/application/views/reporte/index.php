
  
 <!-- Default box -->
  <div class="box">
      <div class="box-header with-border">
          <h3 class="box-title"> <?php echo $title; ?> </h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
      </div>
      <div class="box-body">
        <!-- <div class="col-md-3">
          <div class="form-group">
            <label for="fecha_inicio">Fecha</label>
            <input type="date" class="form-control" id="fecha" value="<?php echo date('Y-m-d');?>" >
          </div>
        </div> -->
        <!-- <div class="col-md-3">
          <div class="form-group">
            <label for="fecha_fin">Fecha Fin</label>
            <input type="date" class="form-control" id="fecha_fin" value="<?php echo date('Y-m-d');?>">
          </div>          
        </div> -->
        <div class="col-md-4">
          <div class="form-group">
            <label for="fecha_fin">Modalidad</label>
            <select class="form-control" id="curso">
			<option value="todo" >Todos</option>
			<? foreach ($resultado as $key => $val) : ?>
			   <option value="<?= $val['id_tipo_matricula']?>" ><?= $val['descripcion']?></option>
			<? endforeach; ?>
              <!-- <option value="General" >General</option>
              <option value="Detallado" >Detallado</option>  -->
            </select>
          </div>          
        </div>
        <div class="col-md-3">
          <br>
          <div class="form-group">
		    <!-- <a  href="<?php echo base_url();?>/reporte/cargar_reporte/1/a"> -->
             <button type="button" class="btn btn-primary" id="boton">Generar Reporte</button>
			<!-- </a> -->
		  </div>
        </div>

        <br>

        <div class="col-md-12 table-responsive" id="conten_reporte">
         
        </div> 
     
      </div>
      <!-- /.box-body -->
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->

<script type="text/javascript">
    base_url = "<?php echo base_url();  ?>";
	$('#boton').on('click', function(){
		// fecha=$('#fecha').val();
		curso=$('#curso').val();
        window.location.href= base_url+"reporte/cargar_reporte/"+curso;
    
    })
</script>