<style type="text/css">
  #div_detalle_venta{
    max-height: 350px;
    overflow-y: scroll;  
  }

  td.prod {
    width: 50% !important;  
  }
  td.cant {
    width: 10% !important;  
  }
  td.prec, td.sub {
    width: 20% !important;  
  }
  td > input {
    width: 100% !important;  
  }

  .select2-container{
    height: auto !important;
  }

  body{
    padding: 0px !important;
  }


</style>


<div class="row" id="form-venta">
  <!-- left column -->
  <div class="col-md-8">
    <div class="box box-success">
      <div class="box-header with-border">
        <div class="input-group input-lg">
          <!--span class="input-group-addon">
            <input type="checkbox" checked="checked" id="tipo_busqueda">
          </span-->
          <input class="form-control input-lg" id="input_buscar_codigo" type="text" placeholder="Buscar codigo barra..." autofocus="autofocus" tabindex="1">
          <select class="form-control select2" id="input_buscar_producto" tabindex="-1">
            <option></option>
          </select>  
          <span class="input-group-addon">
            <button id="get_lista_productos" type="button" class="btn btn-info btn-flat" onclick="get_lista_productos()" tabindex="-1"><i class="fa fa-refresh"></i></button>
          </span>
        </div>
      </div>
      <div class="box-body">

        <div id="div_detalle_venta">
          <table class="table table-striped" id="table_detalle_venta">
            
            <tbody>   
              <!--DETALLE DE VENTA-->
              
            </tbody>
          </table>
        </div>  
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div class="row">

          <div class="col-xs-4">
            <p># Articulos : <span id="cantidadItem">0</span></p>
          </div>
          <div class="col-xs-4 ">
            <!--h3 class="box-title" id="text_total_venta" > TOTAL S/. <span id="total_venta">0.00</span></h3-->
            <div class="form-group">
              <label for="text_total_venta">TOTAL      </label>
              <h3>S/. <span id="total_venta">0.00</span></h3>
            </div>
          </div>  
          <div class="col-xs-4 ">
            <div class="form-group">
              <label for="efectivo">Efectivo</label>
              <input type="number" class="form-control" id="efectivo" placeholder="efectivo" tabindex="2">
            </div>
          </div>
        </div>
        
        <p><code>F9: Guardar venta</code></p>
        <p><code>Ctrl + B: Busqueda rápida</code></p>
      </div>

    </div>
  </div>

  <div class="col-md-4">
    <div class="box box-info">     

      <form class="form-horizontal">
        <div class="box-body">
          <div class="form-group">
            <div class="col-sm-2">
              <label for="fecha_venta" class="control-label">Fecha</label>
              <label for="tienda" class="control-label">Tienda</label>  
            </div>
            <div class="col-sm-10">
              <input type="date" class="form-control" id="fecha_venta" name="fecha_venta" value="<?php echo date('Y-m-d');?>" readonly="readonly">
              <select class="form-control" id="tienda" name="tienda">
                <option value="1"> Tienda 1</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="idserie" class="col-sm-2 control-label">Comp.</label>

            <div class="col-sm-10">
              <select class="form-control" id="idserie" name="idserie" onchange="get_correlativo();"> 
                  <?php foreach($tipo_comprobantes as $tc) {
                    echo "<option value='{$tc->idserie}' > {$tc->tipo_comprobante} </option>";
                  }
                ?>
              </select>
              <input type="text" class="form-control" id="correlativo" name="correlativo" placeholder="Correlativo" readonly="readonly" value="<?php echo $tipo_comprobantes[0]->correlativo; ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <label for="cliente" class="control-label">Cliente</label>
              <label for="dni_cliente" class="control-label">Dni</label>
              <label for="ruc_cliente" class="control-label">Ruc</label>
            </div>  

            <div class="col-sm-10">
              <div class="input-group input-group-sm">
                <input type="text" class="form-control" id="cliente" name="cliente" value="<?php echo $cliente_base[0]->razon_social; ?>">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat" onclick="buscar_cliente()"><i class="fa fa-search"></i></button>
                    </span>
              </div>
                <input type="text" class="form-control" id="dni_cliente" name="dni_cliente" placeholder="Documento" value="<?php echo $cliente_base[0]->dni; ?>">
                <input type="text" class="form-control" id="ruc_cliente" name="ruc_cliente" placeholder="Documento" value="<?php echo $cliente_base[0]->ruc; ?>">
              
            </div>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="dirección_cliente" name="direccion_cliente" placeholder="Dirección" value="<?php echo $cliente_base[0]->direccion; ?>" >
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-4">
              <label for="descuento" class="control-label">SubTotal</label>
              <label for="descuento" class="control-label">IGV</label>
              <label for="descuento" class="control-label">Descuento</label>
            </div>
            
            <div class="col-sm-8">
              <input type="number" class="form-control" id="subtotales" name="subtotales" value="0.00" readonly="readonly" >
              <input type="number" class="form-control" id="igv" name="igv" value="0.00" readonly="readonly" >
              <input type="number" class="form-control" id="descuento" name="descuento" value="0.00" onchange="calcular_total()">
            </div>
          </div>

          <textarea class="form-control" rows="2" placeholder="Observaciones ..." id="observacion" name="observacion"></textarea>
          

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <a href="<?php echo base_url('ventas/lista'); ?>">ir a Lista ventas</a>
          <button type="button" class="btn btn-success pull-right" id="btn_save" onclick="save()">Vender</button>

        </div>
        <!-- /.box-footer -->
      </form>
    </div>

  </div>  

</div>

<script type="text/javascript"> base_url = "<?php echo base_url();  ?>"</script>
