<?php 
    //echo '<pre>';
    //print_r($data);
?>
<table class="table table-bordered">
    <thead>
      <tr>
        <th class="text-center">CANTIDAD</th>
        <th class="text-center">PRESENTACION.</th>
        <th class="text-center">P. COMPRA</th>
        <th class="text-center">P. VENTA</th>
        <th class="text-center">F. MODIFICACION</th>
        <th class="text-center">ESTADO</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $d): ?>
        <tr>
            <td class="text-center"><?= $d->cantidad ?></td>
            <td class="text-center"><?= $d->presentacion ?></td>
            <td class="text-center"><?= $d->precio_compra ?></td>
            <td class="text-center"><?= $d->precio_venta ?></td>
            <td class="text-center"><?= $d->fecha_modificacion ?></td>
            <td class="text-center">
                <?php 
                    if($d->estado == "Activo"){
                        echo "<button type='button' class='btn btn-success btn-xs' >{$d->estado}</button>";
                    } else{
                        echo "<button type='button' class='btn btn-danger btn-xs' >{$d->estado}</button>";
                    }
                ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
