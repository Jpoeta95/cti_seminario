
 <!-- Default box -->
  <form role="form" >
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Producto</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                          title="Collapse">
                    <i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body" > 
                <div class="col-md-12 col-sm-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Producto</label>
                        <div class="input-group input-group-sm">
                            <select class="form-control" id="input_buscar_producto" onchange="table()">
                                <option value="0"></option>
                            </select>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat" onclick="get_lista_productos()">
                                    <i class="fa fa-refresh"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                
            </div>
            
            
         
            <!-- /.box-body -->
          <!-- /.box-footer-->
        </div>
    </div>
    <div class="col-md-12" id="seccion_precios" style="display:none">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Historial de Precios</h3>  
            </div>
            <div class="box-body" > 
                <div class="form-group">
                    <label for="exampleInputEmail1">Precio de Venta</label>
                    <div class="input-group input-group-sm">
                        <input type="number" class="form-control" name="precio_venta" id="precio_venta">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-info btn-flat" onclick="save_precio()">
                                <i class="fa fa-plus"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <div class="table-responsive" id="table_precios">
                    
                </div>
                
            </div>

        </div>
    </div>
</div>
      

</form>

  <!-- /.box -->

<script type="text/javascript"> base_url = "<?php echo base_url();  ?>"</script>
