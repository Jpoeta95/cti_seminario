
<div class="row">
  
    <div class="col-md-3">
    
      <div class="box box-solid">
          
          
            <div class="box-header with-border">
              <h3 class="box-title">Busqueda</h3>
            </div>
            <div class="box-body">
                
                 <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Filtros</a></li>
              <!--li><a href="#tab_2" data-toggle="tab">Producto</a></li-->
            </ul>
                     
                     <form id="formfilter">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                  
                    <div class="form-group">
                        <label for="inputPassword3" class="control-label">Tienda </label> 
                        <select class="form-control" id="tienda_cf" name="tienda_cf">
                            <?php foreach ($tiendas as $tienda): ?>
                            <option value="<?= $tienda->idtienda ?>"><?= $tienda->descripcion ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                
                     <div class="form-group">
                        <label for="inputPassword3" class="control-label">Categoria </label>
                        <select class="form-control" id="listCategorias" name="listCategorias">
                            <option value="">Seleccione...</option>
                             <?php foreach ($categorias as $categoria): ?>
                                <option value="<?= $categoria->idcategoria ?>"><?= $categoria->nombre ?></option>
                             <?php endforeach; ?>
                        </select>
                            <!--div class="input-group input-group-sm">
                                <input type="text" class="form-control"  placeholder="Categoria">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-info btn-flat" onclick="cargaCategorias()"><i class="fa fa-search"></i></button>
                                </span>
                            </div-->

                    
                     </div>
                  
                      <div class="form-group">
                        <label for="inputPassword3" class="control-label">Marca </label>

                        <select class="form-control" id="listMarcas" name="listMarcas">
                            <option value="">Seleccione...</option>
                             <?php foreach ($marcas as $marca): ?>
                                <option value="<?= $marca->idmarca ?>"><?= $marca->nombre ?></option>
                             <?php endforeach; ?>
                        </select>

                    
                     </div>
                  <div class="text-center">
                        
                        <button type="button" class="btn btn-success" onclick="obtenerProductos_cf()">Ver Stock</button>
                  </div>
                  <br>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  
                 <div class="form-group">
                        <label for="inputPassword3" class="control-label">Tienda </label>

                        
                            <select class="form-control" id="tienda_sf" name="tienda_sf">
                                <?php foreach ($tiendas as $tienda): ?>
                                <option value="<?= $tienda->idtienda ?>"><?= $tienda->descripcion ?></option>
                                <?php endforeach; ?>
                              </select>
                                
                            

                    
                     </div>
                  
                  
                <div class="form-group">
                        <label for="inputPassword3" class="control-label">ID Barras</label>

                          <input type="text" class="form-control" id="codbarras" name="codbarras" placeholder="Codigo Barras">
                            
                </div>
                  
                  <div class="text-center">
                      
                    <button type="button" class="btn btn-success" onclick="obtenerProductos_sf">Ver Stock</button>
              </div>
                  
                  <br>
                
              </div>
             
            </div>
            <!-- /.tab-content -->
            </form>
          </div>
          <!-- nav-tabs-custom -->
            
         </div>
      <!-- /.box-body -->
            
    </div>
  </div>
    
    
  <div class="col-md-9">
    
      <div class="box box-success">
          
        <form class="form-horizontal">
          
            <div class="box-header with-border">
              <h3 class="box-title">Stock de Productos</h3>
            </div>
            <div class="box-body">

          
                <div class="box-body">
                  
                    <div id="showtable"></div>

                </div>
                

      </div>
      </form>
      <!-- /.box-body -->
    </div>
  </div>
    
    
</div>


<!-- MODAL CATEGORIAS -->
<div class="modal fade" id="modal-categorias">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Categoria de Productos</h4>
            </div>
            <div class="modal-body">
                <table id="table_categorias" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>Nombre</th>
                      <th>Descripcion</th>
                      <th>Cod SUNAT</th>
                    </tr>
                    </thead>
                    <tbody>
                    

                    </tbody>
                    <tfoot>
                    <tr>
                      <th>Nombre</th>
                      <th>Descripcion</th>
                      <th>Cod SUNAT</th>
                    </tr>
                    </tfoot>
                  </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- MODAL MARCAS -->
<div class="modal fade" id="modal-marcas">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Categoria de Productos</h4>
            </div>
            <div class="modal-body">
                <table id="marcas" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>Rendering engine</th>
                      <th>Browser</th>
                      <th>Platform(s)</th>
                      <th>Engine version</th>
                      <th>CSS grade</th>
                    </tr>
                    </thead>
                    <tbody>
                    

                    </tbody>
                    <tfoot>
                    <tr>
                      <th>Rendering engine</th>
                      <th>Browser</th>
                      <th>Platform(s)</th>
                      <th>Engine version</th>
                      <th>CSS grade</th>
                    </tr>
                    </tfoot>
                  </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script type="text/javascript"> base_url = "<?php echo base_url();  ?>"</script>