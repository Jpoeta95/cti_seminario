var lista_productos ;

$.fn.delayPasteKeyUp = function(fn, ms)
{
	var timer = 0;
	$(this).on("propertychange input", function()
	{
	  clearTimeout(timer);
	  timer = setTimeout(fn, ms);
	});
};

function get_lista_productos(){
	$('#get_lista_productos>i').addClass('fa-spin');
	$.ajax({
        url: base_url + "ventas/lista_productos",
        type: 'GET',
        dataType: 'JSON',
        async: true,
        success: function (data) {
            lista_productos = data;
            $('#input_buscar_producto').select2({
		        placeholder: "Buscar producto",
		        data : lista_productos

		    });
		    $('#get_lista_productos>i').removeClass('fa-spin');
        },
        error: function (data) {
        	$('#get_lista_productos>i').removeClass('fa-spin');
        }

    });
} 

function get_correlativo(){
	idserie= $('#idserie').val();
	$.ajax({
        url: base_url + "ventas/correlativo",
        type: 'GET',
        data: {idserie:idserie},
        dataType: 'JSON',
        async: true,
        success: function (data) {
            $('#correlativo').val(data[0].correlativo);
        }
    });
}

function buscar_cliente(){
	cliente= $('#cliente').val();
	$.ajax({
        url: base_url + "ventas/buscar_cliente",
        type: 'GET',
        data: {valor:cliente},
        dataType: 'JSON',
        async: true,
        success: function (data) {
        	if( data.length == 1){
        		$('#cliente').val(data[0].razon_social);
        		$('#dni_cliente').val(data[0].dni);
        		$('#ruc_cliente').val(data[0].ruc);
        		$('#dirección_cliente').val(data[0].direccion);

        	}else{
        		alerta("Cliente no encontrado","No se encontró registro de cliente",'warning');
        	}            
        }
    });
}  

$(document).ready(function () {
	get_lista_productos();	//actualiza select

	get_correlativo();

	$("#input_buscar_codigo").delayPasteKeyUp(function(){
		buscar_producto_rapido($('#input_buscar_codigo').val());
		$('#input_buscar_codigo').select();
 	}, 200);


 	$('#input_buscar_producto').on('select2:select', function (e) {
	    var data = e.params.data;
	    $('#input_buscar_producto').val(null).trigger('change');
	    add_detalle_venta(data);
	});
	
	shortcut.add("F9",function() {
		save();
	});
	shortcut.add("Ctrl+b",function() {
		$("#input_buscar_codigo").focus();
	});
	$("#efectivo").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code==13){
            save();
        }
    });

	/*//Pedido Ajax select2 --mefalta crear el filtro get(term)
	$('#input_buscar_producto').select2({
		ajax: {
			url: base_url + "ventas/lista_productos",
    		dataType: 'json',
    		delay: 250 ,
    		processResults: function (data) {
		      // Tranforms the top-level key of the response object from 'items' to 'results'
		      return {
		        results: data
		      };
		    }
    	}
	});*/
 	 	

});

function buscar_producto_rapido(valor){	
	result = jQuery.grep(lista_productos, function (producto) { return producto.codbarra == valor });
	if(result.length){
		add_detalle_venta(result[0]);
	}
}

function crea_td( clase, input,value){
	td = '<td '
	td += ( clase === '' ) ? '>' : 'class=\''+clase+'\' >';

	if( input === '' ){
		td += value;
	}else {
		if( clase === 'cant' || clase === 'prec'  ){
			td += '<input onkeyup=\'calcular_subtotal(this)\' onchange=\'calcular_subtotal(this)\' class=\''+clase+'\' name=\''+clase+'[]\' type=\''+input+'\' value=\''+value+'\' >';
		}else{
			td += '<input class=\''+clase+'\'  type=\''+input+'\' value=\''+value+'\' >';
		}	
	}
	td += '</td>'
	return td;
}

function add_detalle_venta(prod){//agrega detalle o aumenta la cantidad
	
	if($("tr[id_prod='"+prod.id+"']").length)
	{
		inp_cant = $('tr[id_prod='+prod.id+']>td.cant>input');
		inp_prec = $('tr[id_prod='+prod.id+']>td.prec>input');
		inp_sub = $('tr[id_prod='+prod.id+']>td.sub>input');

		new_cant = parseInt(inp_cant.val()) + 1;
		inp_cant.val(new_cant); 

		sub = parseInt(new_cant) * parseFloat(inp_prec.val());
		inp_sub.val( parseFloat(sub).toFixed(2) );

	}else{
		idp = prod.id;	
		html = "<tr class=\'item\' id_prod=\'"+idp+"\' >";
		html += crea_td('del','',"<button class=\'btn btn-danger\' onclick=\'remover_detalle_venta("+idp+")\'>X</button>"); ;
		html += crea_td('prod','',prod.text+"<input type=\'hidden\' name=\'idprod[]\' value=\'"+idp+"\'>"+"<input type=\'hidden\' name=\'prodtext[]\' value=\'"+prod.text+"\'>");
		html += crea_td('cant','number',1);//cantidad por defecto
		html += crea_td('prec','number',prod.precio) + crea_td('sub','number\' readonly=\'readonly',prod.precio);
		$("#table_detalle_venta > tbody").append(html);
		
	    $('#cantidadItem').html($('.item').length);

	}		
	calcular_total();
	//$("#modal_pedido-body").html(info_pedido);
}

function remover_detalle_venta(idproducto){
	$('tr[id_prod='+idproducto+']').remove();
	$('#cantidadItem').html($('.item').length);
	calcular_total();
}

var calcular_subtotal = function(element){
    
    var fila=$(element).parent().parent();
    
    var cantidad=fila.find("input[class='cant']").val();
    var pv=fila.find("input[class='prec']").val();

    pv = isNaN(pv) || pv=='' ?0:parseFloat(pv);        
    cantidad = isNaN(cantidad) || cantidad=='' || !Number.isInteger(parseFloat(cantidad))?0:parseFloat(cantidad); 

    var subtotal = pv * cantidad ; 
    fila.find("input[class='sub']").val(subtotal.toFixed(2));  
    calcular_total(); 
}

var calcular_total=function(){
    var subtotales = 0;
    var descuento = $('#descuento').val();
    var total = 0;
    $("input[class='sub']").each(function(){
        var st_det = $(this).val();
        if (!isNaN(st_det) && st_det!=''){
            subtotales=parseFloat(subtotales)+parseFloat(st_det);
        }        
    });  
    total = parseFloat(subtotales).toFixed(2) - parseFloat(descuento).toFixed(2) ;

    $("#subtotales").val(parseFloat(subtotales).toFixed(2));  
    $("#total_venta").html(parseFloat(total).toFixed(2));   

    return total;
}

var save = function(){

	var fallas, total;
	//Deshabilitar boton

	if($("#btn_save").is(":disabled") ){
            alerta("Procesando Venta","En estos momentos se esta procesando la venta",'warning');
            return 0;
        }else{
            $("#btn_save").attr({'disabled':'disabled'});
        }
    

    //Validar campos 
    fallas=true;
    
    if(
            $("#tienda").val()=="" || 
            $("#fecha_almacen").val() == "" ||
            $("#idproveedor").val() == "" ||
            $("#idserie").val() == "" ||
            $("#correlativo").val() == ""
    ){
        alerta("Campos en Blanco","Llene todos los Campos",'warning');

    }else{
        if($("tr[class='item']").length == 0){
            alerta("No hay Productos","La venta no tiene Productos",'warning');
        }else{

        	total = calcular_total();
        	efectivo = $("#efectivo").val();
        	efectivo = (efectivo === '')? total : efectivo ;
        	if(  total > efectivo   ){
        		alerta("Efectivo incorrecto ","El monto de efectivo es menor al total",'warning');
        	}else if ( 	($("#idserie").val() == 2 &&
        				 $("#correlativo").val().length != 11 ) || 
        				($("#idserie").val() == 3 &&
        				 $("#correlativo").val().length != 8 ) ) {
        		alerta("DNI o RUC incorrecto ","El tipo de documento debe corresponder al comprobante",'warning');
        	}
        	else{
        		fallas=false;
        	}
        	
        }
    }

    
    
    if(!fallas){
    		
    	vuelto = parseFloat(efectivo).toFixed(2) - parseFloat(total).toFixed(2) ;

		text_carga = "<h4>Total de venta :  S/."+parseFloat(total).toFixed(2)+"</h4>";
		text_carga += "<h4>Efectivo entregado : S/."+parseFloat(efectivo).toFixed(2)+"</h4>";
		text_carga += "<h3>Vuelto : S/."+parseFloat(vuelto).toFixed(2)+"</h3>";

		dialog_carga = bootbox.alert({
		    message: text_carga+"<p id='carga_venta'> <i class='fa fa-spin fa-spinner'></i> Guardando Venta ...</p>"
		    //closeButton: false
		});

		$.ajax({
	        type: 'POST',
	        url: base_url +"ventas/save",
	        data:$("#form-venta").find("select, textarea, input").serialize(),
	        dataType: 'json',
	        success: function (result) {
	        	if(result){
	        		dialog_carga.find('#carga_venta').html("<p>Venta correcto</p>");
			        setTimeout(function(){
							$(location).attr('href',base_url +"ventas/add");    	
					}, 5000);
	        	}else{
	        		alerta("Fallo al guardar","La venta no fue guardada.",'danger');
	        	}
	        	
	            
	        },
	        error:function (result) {
	        	alerta("Error al guardar","La venta no fue guardada.",'danger');
	        },
	        complete:function (result){
	        	$("#btn_save").removeAttr('disabled');
	        }

	    });	


    }else{
    	$("#btn_save").removeAttr('disabled');
    }

    //Validar segun el comprobante


    
}

