var lista_productos;

$(document).ready(function () {
    get_lista_productos();
});

function get_lista_productos(){
    $.ajax({
        url: base_url + "ventas/lista_productos",
        type: 'GET',
        dataType: 'JSON',
        async: true,
        success: function (data) {
            lista_productos = data;
            $('#input_buscar_producto').select2({
                data : lista_productos
            });
        }
    });
} 

function table(){
    id = $('#input_buscar_producto').val(); 
    if(id!=0){
        refresh_table(id);
        $("#seccion_precios").show();
    }else{
         $("#seccion_precios").hide();
    }
}
function refresh_table(id){
    $.get(base_url + "precios/table_historial/"+id,function (data){
        $("#table_precios").empty().html(data);    
    }); 
}
function save_precio(){
    precio = $('#precio_venta').val();
    id = $('#input_buscar_producto').val(); 
    if(precio!=""){
        $.ajax({
            type: 'POST',
            url: base_url +"precios/save",
            data:{
                "precio":precio,
                "idprod":id
            },
            dataType: 'json',
            success: function (result) {
                alerta("Precio Guardado","Registro Guardado con Exito","success");
                $('#precio_venta').val("");
                $('#precio_venta').focus();
                refresh_table(id)
            },
            error:function (result) {
                    console.log(1);
            }
        });
    }
    
}