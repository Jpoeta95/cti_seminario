
$(document).ready(function () {

    $('.fa-close').on('click', function (e){

        e.preventDefault();

        href = $(this).attr('href'); 

        bootbox.confirm({
            message: "Seguro de eliminar registro?",
            buttons: {
                cancel: {
                    label: 'No',
                    className: 'btn-default'
                },
                confirm: {
                    label: 'Yes',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                
                if (result)   {  
                   location.href = href;
                }else{
                    console.log('cancelado');
                }
            }
        });


    })

	
});

