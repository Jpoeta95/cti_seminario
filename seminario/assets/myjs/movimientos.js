var lista_productos ;

$.fn.delayPasteKeyUp = function(fn, ms)
{
	var timer = 0;
	$(this).on("propertychange input", function()
	{
	  clearTimeout(timer);
	  timer = setTimeout(fn, ms);
	});
};

$(document).ready(function () {

    get_lista_productos();
    get_correlativo();
    $("#input_buscar_codigo").delayPasteKeyUp(function(){
        buscar_producto_rapido($('#input_buscar_codigo').val());
        $('#input_buscar_codigo').select();
    }, 200);
    
    $('#input_buscar_producto').on('select2:select', function (e) {
        var data = e.params.data;
        $('#input_buscar_producto').val(null).trigger('change');
        add_detalle_venta(data);
    });
    
    shortcut.add("F9",function() {
        save();
    });
    shortcut.add("Ctrl+b",function() {
        $("#codbarras").focus();
    });
    
});

function get_lista_productos(){
    $.ajax({
        url: base_url + "ventas/lista_productos",
        type: 'GET',
        dataType: 'JSON',
        async: true,
        success: function (data) {
            lista_productos = data;
            $('#input_buscar_producto').select2({
                data : lista_productos
            });
        }
    });
}
function get_correlativo(){
    tipomovimiento= $('#tipomovimiento').val();
    $.ajax({
        url: base_url + "movimientos/correlativo",
        type: 'GET',
        data: {tipomovimiento:tipomovimiento},
        dataType: 'JSON',
        async: true,
        success: function (data) {
          
            t = (tipomovimiento == "Ingreso")? "1":"2";
            correlativo = "00"+t+"-";
            if(data.length>0){
                 correlativo+=(parseInt(data[0].correlativo)+1);   
                 $('#nrocomprobante').val(correlativo);
            }else{
                correlativo+="1";
                $('#nrocomprobante').val(correlativo);
            }

        }
    });
}


function buscar_producto_rapido(valor){	
	result = jQuery.grep(lista_productos, function (producto) { return producto.codbarra == valor });
	if(result.length){
		add_detalle_venta(result[0]);
	}
}

function crea_td( clase, input,value){
	td = '<td '
	td += ( clase === '' ) ? '>' : 'class=\''+clase+'\' >';

	if( input === '' ){
		td += value;
	}else {
		if( clase === 'cant' || clase === 'prec'  ){
			td += '<input onkeyup=\'calcular_subtotal(this)\' onchange=\'calcular_subtotal(this)\' class=\''+clase+'\' name=\''+clase+'[]\' type=\''+input+'\' value=\''+value+'\' >';
		}else{
			td += '<input class=\''+clase+'\'  type=\''+input+'\' value=\''+value+'\' >';
		}	
	}
	td += '</td>'
	return td;
}

function add_detalle_venta(prod){//agrega detalle o aumenta la cantidad
	
	if($("tr[id_prod='"+prod.id+"']").length)
	{
		inp_cant = $('tr[id_prod='+prod.id+']>td.cant>input');
		inp_prec = $('tr[id_prod='+prod.id+']>td.prec>input');
		inp_sub = $('tr[id_prod='+prod.id+']>td.sub>input');

		new_cant = parseInt(inp_cant.val()) + 1;
		inp_cant.val(new_cant); 

		sub = parseInt(new_cant) * parseFloat(inp_prec.val());
		inp_sub.val( parseFloat(sub).toFixed(2) );

	}else{
		idp = prod.id;	
		html = "<tr class=\'item\' id_prod=\'"+idp+"\' >";
		html += crea_td('del','',"<button class=\'btn btn-danger\' onclick=\'remover_detalle_venta("+idp+")\'>X</button>"); ;
		html += crea_td('prod','',prod.text+"<input type=\'hidden\' name=\'idprod[]\' value=\'"+idp+"\'>"+"<input type=\'hidden\' name=\'prodtext[]\' value=\'"+prod.text+"\'>");
		html += crea_td('cant','number',1);//cantidad por defecto
		html += crea_td('prec','number\' readonly=\'readonly',prod.precio) + crea_td('sub','number\' readonly=\'readonly',prod.precio);
		$("#table_detalle_venta > tbody").append(html);
		
	    $('#cantidadItem').html($('.item').length);

	}		
	calcular_total();
	//$("#modal_pedido-body").html(info_pedido);
}

function remover_detalle_venta(idproducto){
	$('tr[id_prod='+idproducto+']').remove();
	$('#cantidadItem').html($('.item').length);
	calcular_total();
}

var calcular_subtotal = function(element){
    
    var fila=$(element).parent().parent();
    
    var cantidad=fila.find("input[class='cant']").val();
    var pv=fila.find("input[class='prec']").val();

    pv = isNaN(pv) || pv=='' ?0:parseFloat(pv);        
    cantidad = isNaN(cantidad) || cantidad=='' || !Number.isInteger(parseFloat(cantidad))?0:parseFloat(cantidad); 

    var subtotal = pv * cantidad ; 
    fila.find("input[class='sub']").val(subtotal.toFixed(2));  
    calcular_total(); 
}

var calcular_total=function(){
    var subtotales = 0;
    var total = 0;
    $("input[class='sub']").each(function(){
        var st_det = $(this).val();
        if (!isNaN(st_det) && st_det!=''){
            subtotales+=parseFloat(st_det);
        }        
    });  
    total = parseFloat(subtotales).toFixed(2);

    $("#totalmovimiento").val(parseFloat(subtotales).toFixed(2));  
    $("#total_venta").text(parseFloat(total).toFixed(2));   

    return total;
}


var save = function(){
    
    var fallas, total;
    //Deshabilitar boton
    $("#btn_save").attr({'disabled':'disabled'});

    //Validar campos 
    fallas=true;
    
    if($("tr[class='item']").length == 0){
        alerta("No hay Productos","La venta no tiene Productos",'warning');
    }else{
        fallas = false;
    }
    
    
    
    if(!fallas){
    		
    	
        $.ajax({
            type: 'POST',
            url: base_url +"movimientos/save",
            data:$("#form-venta").find("select, textarea, input").serialize(),
            dataType: 'json',
            success: function (result) {
                if(result){
                    alerta("Movimiento Exitoso","Se guardo correctamente el Movimiento",'success');
                    
                    setTimeout(function(){
                        window.location.replace(base_url +"movimientos/lista");
                    }, 1700);
                }else{
                    alerta("Fallo al guardar","La venta no fue guardada.",'danger');
                }
            },
            error:function (result) {
                    alerta("Error al guardar","La venta no fue guardada.",'danger');
            }
        });		
    	
    }

    //Validar segun el comprobante


    $("#btn_save").removeAttr('disabled');
}
