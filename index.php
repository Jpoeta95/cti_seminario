<?php 

	include('config.php');

	include('cabecera.php');

	$sliders_web  = array(  
							array('img' => 'images/unsm-tarapoto.jpg',
								  'texto' => '<span> II  SEMINARIO INTERNACIONAL <br> "Aplicabilidad de las TIC\'S en la gestión del conocimiento"</span> ' ),
							array('img' => 'images/unsm.jpg',
								  'texto' => ""),

					);

	$promocion =  array(
		'titulo' => 'Registrate ahora y obten<span>10%</span> descuento hasta el 01 de Octubre', 
		'texto' => 'Aprovecha esta gran oportunidad, mejora tu habilidades y añade conocimientos a tu curriculum.',
		'boton' => 'Matricularse ahora'
	);

	$aliados_web =  array(
		'images/aliados/CITE_ACUICOLA.png',
		'images/aliados/ITP.png',
		'images/aliados/ku-logo.png',
		'images/aliados/SENASA.png',
		'images/aliados/LOGO_UCV.png',
		'images/aliados/logo_upn.png',
		'images/aliados/T_Solutions_INC.png',
		'images/aliados/Logo_eScire_Slogan.png',
		'images/aliados/UNSA.png',
		'images/aliados/UPEU.png',
		'images/aliados/UOC.png',

	);

?>
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="styles/responsive.css">
<style type="text/css">
	.img_aliado{
		width: 90%;
		margin: auto !important;
	}

	.contenedor:hover .imagen {-webkit-transform:scale(1.3);transform:scale(1.3);}
	.contenedor {overflow:hidden;}

	.facebook-responsive {
    overflow:hidden;
    padding-bottom:56.25%;
    position:relative;
    height:0;
}

.facebook-responsive iframe {
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
}

#section_title_aux h1::before {
	background:  #ffffff;
}

#section_title_aux h1 {
	color:  #ffffff;
}

</style>

<!-------->
<link rel="stylesheet" type="text/css" href="styles/countdown_styles.css">

	
	<!-- Home -->
	<div class="home">

		<!-- Hero Slider -->
		<div class="hero_slider_container">
			<div class="hero_slider owl-carousel">
				
				<!-- Hero Slide -->
				<? foreach ($sliders_web as $key => $val) : ?>
					<div class="hero_slide">
						<div class="hero_slide_background" style="background-image:url(<?=$val['img']?>);"></div>
						<div class="hero_slide_container d-flex flex-column align-items-center justify-content-center">
							<div class="hero_slide_content text-center" style="min-width: 60%">


								<? 	
									if($val['texto'] == '') :
								?>

									<div class="row Countdown-content" id="Countdown-content">

										<div class="col-xs-3 col-sm-3 col-md-3 Countdown-block Countdown-block--colored">
											<span class="Countdown-label ">Quedan</span>
											<span class="Countdown-label ">solo</span>
										</div>	
										<div class="col-xs-3 col-sm-3 col-md-3 Countdown-block Countdown-block--wSeparator">
											<span class="Countdown-label Countdown-label--main" id="Countdown-dia">0</span>
											<span class="Countdown-label">Días</span>
										</div>
										<div class="col-xs-3 col-sm-3 col-md-3 Countdown-block Countdown-block--wSeparator"> 
											<span class="Countdown-label Countdown-label--main" id="Countdown-hora">0</span>
											<span class="Countdown-label ">Horas</span>
										</div>
										<div class="col-xs-3 col-sm-3 col-md-3 Countdown-block">
											<span class="Countdown-label Countdown-label--main" id="Countdown-minuto">0</span>
											<span class="Countdown-label ">Minutos</span>
										</div>

									</div>	

								<?
									else :
								?>
									<h1 style="font-size: 35px;" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut"><?=$val['texto']?></h1>
								<?
									endif;
								?>
								
								<!-- Milestone -->									


							</div>
						</div>
					</div>

				<? endforeach; ?>

			</div>

			<div class="hero_slider_left hero_slider_nav trans_200"><span class="trans_200">prev</span></div>
			<div class="hero_slider_right hero_slider_nav trans_200"><span class="trans_200">next</span></div>
		</div>

	</div>

	<div class="hero_boxes">
		<div class="hero_boxes_inner">
			<div class="container">
				<div class="row">

					<div class="col-lg-4 hero_box_col" onclick="window.location='formulario_pre.php';">
						<div class="hero_box d-flex flex-row align-items-center justify-content-start">
							<img src="images/earth-globe.svg" class="svg" alt="">
							<div class="hero_box_content">
								<h2 class="hero_box_title">Matricula Online</h2>
								<a href="formulario_pre.php" class="hero_box_link">ver más</a>
							</div>
						</div>
					</div>

					<div class="col-lg-4 hero_box_col" onclick="window.location='talleres.php';">
						<div class="hero_box d-flex flex-row align-items-center justify-content-start">
							<img src="images/books.svg" class="svg" alt="">
							<div class="hero_box_content">
								<h2 class="hero_box_title">Talleres</h2>
								<a href="talleres.php" class="hero_box_link">ver más</a>
							</div>
						</div>
					</div>

					<div class="col-lg-4 hero_box_col" onclick="window.location='ponentes.php';">
						<div class="hero_box d-flex flex-row align-items-center justify-content-start">
							<img src="images/professor.svg" class="svg" alt="">
							<div class="hero_box_content">
								<h2 class="hero_box_title">Ponentes</h2>
								<a href="ponentes.php" class="hero_box_link">ver más</a>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>


	<div class="popular page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Temas Interesantes</h1>
					</div>
				</div>
			</div>

			<div class="row course_boxes">
				
				<!-- Popular Course Item -->
				<? foreach ($cursos_web as $key => $val) : ?>				

				<div class="col-lg-4 course_box">
					<div class="card">
						<img class="card-img-top" src="<?=$val['img']?>" alt="https://www.facebook.com/cti.unsm">
						<div class="card-body text-center">
							<div class="card-title"><a href="talleres.php"><?=$val['card-title']?></a></div>
							<div class="card-text"><?=$val['card-text']?></div>
						</div>

						<? if ( $val['course_author_name'] != '' ):?>
						<div class="price_box d-flex flex-row align-items-center">
							<div class="course_author_image">
								<img src="<?=$val['course_author_image']?>" alt="https://www.facebook.com/cti.unsm">
							</div>
							<div class="course_author_name"><?=$val['course_author_name']?></div>
							<div class="course_price d-flex flex-column align-items-center justify-content-center"><span><?=$val['course_price']?></span></div>
						</div>

						<? endif; ?>
					</div>
				</div>

				<? endforeach; ?>

			</div>
		</div>		
	</div>

	<!-- Register -->

	

	<div class="register">

		<div class="container-fluid">
			
			<div class="row" style="background: #349443;padding: 20px;">

				<div class="col-md-12">
				<div class="section_title text-center" id="section_title_aux">
				<h1>Invitación de Profesionales</h1>
				</div>
				<br>
				</div>

				<? 

				$video_url = array( "https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F802687619822751%2Fvideos%2F389493045052166%2F&show_text=0&width=560",
									"https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F802687619822751%2Fvideos%2F2459113097517577%2F&show_text=0&width=560",
									"https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F802687619822751%2Fvideos%2F400273394239498%2F&show_text=0&width=560",
									"https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F802687619822751%2Fvideos%2F3100615646679369%2F&show_text=0&width=560"

				);

				
				?>

				<? foreach ($video_url as $key => $value) : ?>

					<div class="col-md-6">
						<div class="facebook-responsive">
						    <iframe src="<?= $value ?>" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
						</div>	
						<br>				
					</div>
				
				<? endforeach ?>

				<div class="register_section d-flex flex-column align-items-center justify-content-center">
					<div class="register_content text-center">
						<div class="button button_1 register_button mx-auto trans_200"><a href="formulario_pre.php">Regístrese ya</a></div>
					</div>
				</div>
				
			</div>
		</div>
	</div>


	<div class="events page_section">
		<div class="container">
			
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1> Cronograma del evento</h1>
					</div>
				</div>
			</div>
			
			<div class="event_items">

				<? 

				$cronograma = array( 
									array( 'dia' =>24,
										   'mes'=>'Octubre',
										   'nombre'=> 'II Seminario Internacional',
										   'location'=>'Inicio de ponencias',
										   'sublocation'=>'Auditorio de la Posada – Jr. Oriental 175. Morales',
										   'img'=>'images/event_1.jpg',
										),
									array( 'dia' =>25,
										   'mes'=>'Octubre',
										   'nombre'=> 'II Seminario Internacional',
										   'location'=>'Segundo día de ponencias y Talleres',
										   'sublocation'=>'Auditorio de la FICA – Ciudad Universitaria. Morales',
										   'img'=>'images/unsm.jpg',
										),
									array( 'dia' =>26,
										   'mes'=>'Octubre',
										   'nombre'=> 'II Seminario Internacional',
										   'location'=>'Ultimo Taller',
										   'sublocation'=>'Laboratorios de la FISI – Ciudad Universitaria. Morales.',
										   'img'=>'images/unsm-computo.jpg',
										),
							);
				
				?>



				<?php foreach ($cronograma as $key => $value): ?>

				<div class="row event_item">
					<div class="col">
						<div class="row d-flex flex-row align-items-end">

							<div class="col-lg-2 order-lg-1 order-2">
								<div class="event_date d-flex flex-column align-items-center justify-content-center">
									<div class="event_day"> <?= $value['dia'] ?></div>
									<div class="event_month"><?= $value['mes'] ?></div>
								</div>
							</div>

							<div class="col-lg-6 order-lg-2 order-3">
								<div class="event_content">
									<div class="event_name"><a class="trans_200" href="#"><?= $value['nombre'] ?></a></div>
									<div class="event_location"><?= $value['location'] ?> 
									<a href="web/CRONOGRAMA_SEMINARIO.pdf" download> - (Ver Cronograma.pdf)</a>
									</div>
									<p><?= $value['sublocation'] ?></p>
								</div>
							</div>

							<div class="col-lg-4 order-lg-3 order-1">
								<div class="event_image">
									<img src="<?= $value['img'] ?>" alt="Cti UNSM">
								</div>
							</div>

						</div>	
					</div>
				</div>

				<?php endforeach ?>
				

			</div>
				
		</div>
	</div>

	<!-- Footer -->


<?php 

	include('pie.php');

?>

<script src="js/countdown.js"></script>