
<?php 

	include('config.php');

	// $telefono = '955 941 992';
	$msj= array( 'invitacion' =>"LA UNIVERSIDAD NACIONAL DE SAN MARTÍN TE INVITA A PARTICIPAR DEL SEMINARIO INTERNACIONAL | La aplicabilidad de las TIC's en la gestión del conocimiento." );
	
	$estar_conctacto="Estar en contacto";

	$footer_about_text = "In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum, tempor lacus.";


	$header  = array(  array('Menu', array("index.php","Inicio"),array("#","Nosotros"),array("curso.php","Curso"), array("#","Contacto")),
				   array('Usefull Links', array("Testimonials","FAQ","Community","Campus Pictures","Tuitions")),
				   array('Contacto', array("images/placeholder.svg","Blvd Libertad, 34 m05200 Arévalo"),array("images/smartphone.svg","955 941 992"),array("images/envelope.svg","hello@company.com"))
			   );


	$sql = "SELECT * FROM tipo_matricula ";        
    $result = $conexion->prepare($sql);
    $result->execute(array());   
    $tipo_matricula = $result->fetchAll();

    $sql = "SELECT * FROM cursos ";
    $result = $conexion->prepare($sql);
    $result->execute(array());   
    $cursos = $result->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title><?= $etiqueta_pagina ?></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Course Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/contact_styles.css">
<link rel="stylesheet" type="text/css" href="styles/contact_responsive.css">
<link rel="stylesheet" type="text/css" href="styles/elements_styles.css">
<link rel="stylesheet" type="text/css" href="styles/elements_responsive.css">
<style type="text/css">
	
	#table-costos > tbody > tr >.montos , #table-costos > tbody > tr >.docente {
		text-align: right !important; 
	}

	#table-costos > tbody > tr >.estado {
		text-align: center !important; 
	}

	input[type='radio']:after {
        width: 15px;
        height: 15px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #d1d3d1;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

    input[type='radio']:checked:after {
        width: 20px;
        height: 20px;
        border-radius: 15px;
        top: -2px;
        left: -1px;
        position: relative;
        background-color: #ffa500;
        content: '';
        display: inline-block;
        visibility: visible;
        border: 2px solid white;
    }

    select {
	    /*background: transparent;
	    border: none;*/
	    color: #a5a5b6 !important;
	}

    .send_submit{
    	background: #a8a5a5 !important;
    }

    .mayusculas{
    	text-transform: uppercase;
    }

</style>

</head>
<body>

<div class="super_container">

	

	<!-- Contact -->
	<form action="" class="form-horizontal" method="post" onsubmit="return enviar(this)">

	<div class="contact">
		<div class="container">
			<div class="row">

				<div class="col-lg-12 col-md-12">

					<div class="contact_title" style="text-align: center;"> MATRICÚLATE AHORA <br> <?= $nombre_evento ?></div>

					<div class="about">
						<p class="about_text"><?=$msj['invitacion']?></p>
					</div>

				</div>

				<div class="col-lg-7 col-md-7" id="datos_personales">
					
					<div class="accordion_container">
						<div class="accordion d-flex flex-row align-items-center"> DATOS PERSONALES </div>
						<div class="accordion_panel">
							<br>
							

								<input id="contact_form_name" class="input_field contact_form_name mayusculas" type="text" placeholder="*Ingrese su Nombre" required="required" data-error="Ingrese sus Nombre." name="nombres"  > 

								<input id="contact_form_apellido" class="input_field contact_form_name mayusculas" type="text" placeholder="*Ingrese su Apellido" required="required" data-error="Ingrese sus Apellidos." name="apellidos" > 

								<input id="contact_form_dni" class="input_field contact_form_name" type="text" placeholder="*Ingrese su DNI" required="required" data-error="Ingrese su DNI." name="dni" size="8" pattern="[0-9]{8}"> 

								<input id="contact_form_email" class="input_field contact_form_email" type="email" placeholder="Ingrese su Correo"  data-error="Ingrese su correo." name="correo">

								<input id="contact_form_telefono" class="input_field contact_form_telefono" type="text" placeholder="Ingrese su Telefono"  data-error="Ingrese su telefono." name="telefono">

								<p>*Ingrese correctamente su nombre, ya que será usado para su certificado(s).</p>
		
								
						</div>
					</div>
						
				</div>

				<div class="col-lg-5 col-md-5" id="inversion">

					<div class="elements_accordions">						

						<div class="accordion_container">
							<div class="accordion d-flex flex-row align-items-center"> INVERSIÓN </div>
							<div class="accordion_panel">
								<br>
								<table class="table table-hover" id="table-costos">

									<tbody>

										<? foreach ($tipo_matricula as $key => $val) : ?>	

											<tr >
												<th> 
													<label for='inversion<?= $val['id_tipo_matricula'] ?>'><?= utf8_encode($val['descripcion']) ?></label>
												</th>
												<td class="monto">
													<label for='inversion<?= $val['id_tipo_matricula'] ?>'> <?= $val['precio'] ?></label>
												</td>
												<td>
									                <div class="radio">
									                    <label for='inversion<?= $val['id_tipo_matricula'] ?>'>
									                    	<input type="radio" id='inversion<?= $val['id_tipo_matricula'] ?>' name="id_tipo_matricula" value="<?= $val['id_tipo_matricula'] ?>" required></label>
									                </div>
									            </td>

											</tr>

										<? endforeach; ?>		

									</tbody>
								</table>

								<p>*Cuando se requiera, será necesario adjuntar un documento que convalide su selección.</p>
							</div>
						</div>					

					</div>

				</div>
				

				<!--div class="col-lg-7 col-md-7" id="cursos">

					<div class="elements_accordions">						

						<div class="accordion_container">
							<div class="accordion d-flex flex-row align-items-center"> CURSOS Y TALLERES </div>
							<div class="accordion_panel" style="overflow-y: auto;">
								<br>

								<div class="form-group row">
								    <label for="inputEmail3" class="col-sm-2 col-form-label">Primario</label>
								    <div class="col-sm-10">
								      	<select class="form-control form-control-lg" name="curso_principal" id="curso_principal" onchange='curso_secundarios()' required>
								      		<option value="">--- Seleccione ---</option>
											<? foreach ($cursos as $key => $val) : ?>	
												<option value="<?= $val['id_curso'] ?>" > <?= $val['descripcion'] ?> </option>
											<? endforeach; ?>	
										</select>
								    </div>
								</div>

								<div class="form-group row">
								    <label for="inputEmail3" class="col-sm-2 col-form-label">Secundario</label>
								    <div class="col-sm-10">
								      	<select class="form-control form-control-lg" name="curso_secundario" id="curso_secundario">
								      		<option value="">--- Seleccione ---</option>
											<? foreach ($cursos as $key => $val) : ?>	
												<option value="<?= $val['id_curso'] ?>" > <?= $val['descripcion'] ?> </option>
											<? endforeach; ?>
										</select>
								    </div>
								</div>

								<br>

								<p>*La opcion PRIMARIO, reserva una vacante en el taller seleccionado.<br>
								*La opcion SECUNDARIO, es opcional y reserva una vacante en el taller seleccionado, previa confirmación de los organizadores.</p>

							</div>
						</div>					

					</div>

				</div-->

				

				<div class="col-lg-7 col-md-7" id="forma_pago">

					<div class="elements_accordions">
						
						<div class="accordion_container">
							<div class="accordion d-flex flex-row align-items-center" id="forma_pago"> FORMAS DE PAGO.</div>
							<div class="accordion_panel">
								<br>
								<h3>DEPÓSITO EN SOLES - BBVA. <br>
									Cuenta: 0201107682 <br>
									CCI:	011-310-000201107682-01
								</h3>

								<p>*<strong>Inscripción online</strong>: El voucher de pago debe enviarse al correo ctiunsm@unsm.edu.pe adjuntando un documento que sustente la categoría de la inversión, con esto asegura su inscripción. Evite problemas posteriores. <br>

								*<strong>Inscripción presencial</strong>: Debe acercarse a la oficina del CTI (Jr. Orellana 575 – Tarapoto /Complejo universitario) o el día del evento al ingresar al auditorio de la POSADA (Jr. Oriental N° 175 – Morales), portando el voucher y el documento que sustente la categoría de la inversión. <br>

								*Se entregará comprobante (boleta o factura) según lo requerido en los lugares ya antes mencionado.</p>

								

							</div>
						</div>

					</div>

				</div>

				<div class=" col-lg-5 col-md-5" id="enviar">
					<button id="submit" type="Submit" class="contact_send_btn trans_200" style="height: 60px" >Enviar Solicitud</button>
				
				</div>


			</div>

		</div>
	</div>

	</form>



<?php 

	include('pie.php');

?>

<!-- Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Modal Heading</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        Modal body..
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
      </div>

    </div>
  </div>
</div>


<script src="js/elements_custom.js"></script>
<script type="text/javascript">

	function activa(){

		acc = $('.accordion');//$('#forma_pago');
		acc.toggleClass('active');			
		var panel = $(acc.next());
		var panelH = panel.prop('scrollHeight') + "px";		
		if(panel.css('max-height') == "0px")
		{	panel.css('max-height', panel.prop('scrollHeight') + "px");
		}
		else
		{	panel.css('max-height', "0px");			
		} 

	}

	function curso_secundarios(){
		cp = $('#curso_principal');
		cs = $('#curso_secundario');
		cs.val('');
		$("#curso_secundario > option").show();
		$("#curso_secundario > option[value=" + cp.val() + "]").hide();
	}

	function enviar(){

		myModal = $('#myModal'); 

		$.ajax({

		 	url:'ajax/prematricula.php',
		 	type:'POST',
		 	data: $("form").serialize(), 
		 	dataType: 'json',
		 	beforeSend: function( xhr ) {
		 	   myModal.modal('show');
			   myModal.find('.modal-title').html('Enviando Solicitud ...');
			   myModal.find('.modal-body').html('Cargando ...');
			   myModal.find('.modal-footer').hide();
			},
			success:function(datos){


				myModal.find('.modal-title').html('Respuesta Solicitud ...');
			   	myModal.find('.modal-body').html(datos.mensaje);

			   	myModal.find('.modal-footer').show();
			   	myModal.find('.modal-footer > button').html('Aceptar');

				if(datos.estado == 1){										
					$('#submit').addClass('send_submit').html('SOLICITUD ENVIADA').prop( "disabled", true );
				}
			}


		})

		return false; 
	}



	activa();
	
</script>
