<?php 
	
	
	$unirse_curso="Unirse a cursos";
	$unirse_curso1="In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum. Etiam eu purus nec eros varius luctus. Praesent finibus risus facilisis ultricies. Etiam eu purus nec eros varius luctus.";
	
	$estar_conctacto="Estar en contacto";

	$titulo_curso = 'Cursos Populares';

	$footer_about_text = "In aliquam, augue a gravida rutrum, ante nisl fermentum nulla, vitae tempor nisl ligula vel nunc. Proin quis mi malesuada, finibus tortor fermentum, tempor lacus.";

	
	$menu_social  = array(  'fab fa-pinterest','fab fa-linkedin-in',
						'fab fa-instagram',	'fab fa-facebook-f','fab fa-twitter'
					   );
					   

    $header  = array(  array('Menu', array("index.php","Inicio"),array("#","Nosotros"),array("curso.php","Curso"), array("#","Contacto")),
				   array('Usefull Links', array("Testimonials","FAQ","Community","Campus Pictures","Tuitions")),
				   array('Contacto', array("images/placeholder.svg","Blvd Libertad, 34 m05200 Arévalo"),array("images/smartphone.svg","955 941 992"),array("images/envelope.svg","hello@company.com"))
			   );

	$cursos_web = array(  array('img' => 'images/course_1.jpg',
								'card-title' => "A complete guide to design",
								'card-text' => "Adobe Guide, Layes, Smart Objects etc...",
								'course_author_image' => "images/author.jpg",
								'course_author_name' => "Michael Smith, <span>Author</span>",
								'course_price' => "$29" ) ,

						  array('img' => 'images/course_2.jpg',
								'card-title' => "A complete guide to design 2",
								'card-text' => "Adobe Guide, Layes, Smart Objects etc...",
								'course_author_image' => "images/author.jpg",
								'course_author_name' => "Michael Smith, <span>Author</span>",
								'course_price' => "$29"),

						  array('img' => 'images/course_3.jpg',
								'card-title' => "A complete guide to design",
								'card-text' => "Adobe Guide, Layes, Smart Objects etc...",
								'course_author_image' => "images/author.jpg",
								'course_author_name' => "Michael Smith, <span>Author</span>",
								'course_price' => "$29" ) 
					);
	$canti_mat  = array(  array('img'=>'images/milestone_1.svg','img1'=>'images/milestone_1.svg','cantidad'=>'750','cant_per'=>'Current Students'),
					array('img'=>'images/milestone_2.svg','img1'=>'images/milestone_1.svg','cantidad'=>'120','cant_per'=>'Certified Teachers'),
				   array('img'=>'images/milestone_3.svg','img1'=>'images/milestone_1.svg','cantidad'=>'39','cant_per'=>'Approved Courses'),
				   array('img'=>'images/milestone_4.svg','img1'=>'images/milestone_1.svg','cantidad'=>'3500','cant_per'=>'Graduate Students'),
				   array('img'=>'images/milestone_4.svg','img1'=>'images/milestone_1.svg','cantidad'=>'3500','cant_per'=>'Graduate Students'),
				   array('img'=>'images/milestone_4.svg','img1'=>'images/milestone_1.svg','cantidad'=>'3500','cant_per'=>'Graduate Students'),
				   array('img'=>'images/milestone_4.svg','img1'=>'images/milestone_1.svg','cantidad'=>'3500','cant_per'=>'Graduate Students')
	 );

	include('config.php');

	include('cabecera.php');
				

?>

<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/courses_styles.css">
<link rel="stylesheet" type="text/css" href="styles/courses_responsive.css">
<!-- <link rel="stylesheet" type="text/css" href="styles/main_styles.css"> -->
<link rel="stylesheet" type="text/css" href="styles/teachers_styles.css">
<link rel="stylesheet" type="text/css" href="styles/teachers_responsive.css">
<link rel="stylesheet" type="text/css" href="styles/news_styles.css">

	<!-- Home -->

	<div class="home">
		<div class="home_background_container prlx_parent">
			<div class="home_background prlx" style="background-image:url(images/unsm-computo.jpg)"></div>
		</div>
		<div class="home_content">
			<h1>Cursos</h1>
		</div>
	</div>

	<!-- Popular -->

	<div class="milestones">
	    <div class="milestones_background"></div>
		<!-- <div class="row"> style="background-image:url(images/milestones_background.jpg)"-->
				<!-- <div class="col"> -->
					<div class="milestone_text_conteo text-center">
						<h1 style="color: #349443">Estudiantes matriculados</h1>
					</div>
				<!-- </div> -->
		<!-- </div> -->
		<div class="container">
			<div class="row">
				
				<!-- Milestone -->
				<? foreach ($canti_mat as $key => $val) : ?>
					<div class="col-lg-3 milestone_col" style="margin-bottom: 20px;">
						<div class="milestone text-center">
							<div class="milestone_icon"><img src="<?=$val['img']?>"></div> <div class="milestone_icon"><img src="<?=$val['img1']?>"></div>
							<div class="milestone_counter" data-end-value="<?=$val['cantidad']?>">0</div>
							<div class="milestone_text_curso"><?=$val['cant_per']?></div>
						</div>
					</div>
				<? endforeach ?>
			</div>
		</div>
	</div>

<?
	include('piepage.php');
?>

<script src="js/courses_custom.js"></script>