<?php 

	include('config.php');

	include('cabecera.php');

	$enlaces_web = array( array( 'texto'=>'Pagina web de UNSM', 'link'=> 'https://unsm.edu.pe'),
						  array( 'texto'=>'Pagina web de CPU - UNSM', 'link'=> 'http://cpu.unsm.edu.pe/'),
						  array( 'texto'=>'Centro de Idiomas', 'link'=> 'https://unsm.edu.pe/centros-de-produccion/idiomas/'),
						  array( 'texto'=>'Investigación y desarrollo', 'link'=> 'https://unsm.edu.pe/investigacion/'),
				);

	$etiquetas_web = array( 'Informatica', 'Autocad', 'TIC\'s' , 'Talleres');


?>

<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="styles/news_styles.css">
<link rel="stylesheet" type="text/css" href="styles/news_responsive.css">

	
	<!-- Home -->
	<div class="home">
		<div class="home_background_container prlx_parent">
			<div class="home_background prlx" style="background-image:url(<?= $imagenes_web['fondo']['nosotros'] ?>)"></div>
		</div>
		<div class="home_content">
			<h1>Nosotros</h1>
		</div>
	</div>


	<!-- News -->
	<div class="news">
		<div class="container">
			<div class="row">
				
				<!-- News Column -->

				<div class="col-lg-8">
					
					<div class="news_posts">
						<!-- News Post -->
						<div class="news_post">
							
							<div class="news_post_top d-flex flex-column flex-sm-row">
								<div class="news_post_date_container">
									<div class="news_post_date d-flex flex-column align-items-center justify-content-center">
										<div>UNSM</div>
									</div>
								</div>
								<div class="news_post_title_container">
									<div class="news_post_title">
										<a href="#">Universidad Nacional de San Martin</a>
									</div>									
								</div>
							</div>
							<br>
							<div class="news_post_image">
								<img src="images/unsm-computo.jpg" alt="https://unsplash.com/@dsmacinnes">
							</div>
							<div class="news_post_text">
								<p>Centro Superior de Estudios autónoma y de carácter estatal, comprometida con la formación de profesionales humanistas y competitivos, con responsabilidad social y comprometidos con el desarrollo local, regional y nacional, mediante la generación de conocimientos, tecnologías e innovación, en el marco de una cultura de valores, en proceso de acreditación y de actualización permanente.</p>
							</div>

							<div class="news_post_top d-flex flex-column flex-sm-row">
								<div class="news_post_date_container">
									<div class="news_post_date d-flex flex-column align-items-center justify-content-center">
										<div>UNSM</div>
									</div>
								</div>
								<div class="news_post_title_container">
									<div class="news_post_title">
										
										<a href="#">VISIÓN</a>
									</div>									
								</div>
							</div>
							<div class="news_post_text">
								<p>Formar profesionales competitivos e innovadores en favor de los estudiantes universitarios basados en la <span style="text-decoration-color: red ;"> investigación científica, tecnológica y humanística,</span> comprometida en la mejora continua de la calidad y la responsabilidad social.</p>
							</div>

							<div class="news_post_top d-flex flex-column flex-sm-row">
								<div class="news_post_date_container">
									<div class="news_post_date d-flex flex-column align-items-center justify-content-center">
										<div>UNSM</div>
									</div>
								</div>
								<div class="news_post_title_container">
									<div class="news_post_title">
			
										<a href="#">MISIÓN</a>
									</div>									
								</div>
							</div>
							<div class="news_post_text">
								<p>Los peruanos acceden a una educación que les permite desarrollar su potencial desde la primera infancia y convertirse en ciudadanos que valoran su cultura, conocen sus derechos y responsabilidades, desarrollan sus talentos y participan de manera innovadora, competitiva y comprometida en las dinámicas sociales contribuyendo al desarrollo de sus comunidades y del país en su conjunto.</p>
							</div>

						</div>

					</div>


				</div>

				<!-- Sidebar Column -->

				<div class="col-lg-4">
					<div class="sidebar">

						<!-- Archives -->
						<div class="sidebar_section">
							<div class="sidebar_section_title">
								<h3>Enlaces</h3>
							</div>
							<ul class="sidebar_list">

								<? foreach ($enlaces_web as $key => $val) : ?>
									<li class="sidebar_list_item"><a href="<?= $val['link']?>"><?= $val['texto']?></a></li>
								<? endforeach; ?>
							</ul>
						</div>

						<!-- Latest Posts -->

						<div class="sidebar_section">
							<div class="sidebar_section_title">
								<h3>Siguenos</h3>
							</div>
							<div id="fb-root"></div>
                            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v4.0"></script>
							<div class="fb-page" data-href="https://www.facebook.com/Centro-en-Tecnolog&#xed;as-de-informaci&#xf3;n-802687619822751/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Centro-en-Tecnolog&#xed;as-de-informaci&#xf3;n-802687619822751/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Centro-en-Tecnolog&#xed;as-de-informaci&#xf3;n-802687619822751/">Centro en Tecnologías de información</a></blockquote></div>
							
							<!-- <div class="latest_posts">
							      <img class="img-aliados" src="images/facebook.jpg" alt="https://www.facebook.com/cti.unsm">
							</div> -->
								
						</div>

						<!-- Tags -->

						<div class="sidebar_section">
							<div class="sidebar_section_title">
								<h3>Etiquetas</h3>
							</div>
							<div class="tags d-flex flex-row flex-wrap">
								<? foreach ($etiquetas_web as $key => $val) : ?>
									<div class="tag"><a href="#"><?= $val ?></a></div>	
								<? endforeach; ?>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	
	

<?
	include('piepage.php');

?>