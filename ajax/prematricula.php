<?php 


    date_default_timezone_set("America/Lima");

    $Servidor = "149.56.134.245";//"149.56.134.245";////"localhost";//"192.168.1.39";//
    $Puerto   = "3306"; //"5432"; //"5434";
    $Usuario  = "admin_default"; //"corp"; //"postgres";
    $Password = "admin6"; //"@dmin$7391&"; //$CriptF;//
    $Base     = "admin_cti"; //"corp_sicuani"; //"e-siincoweb_empssapal";
  
   $conexion;

    try {
        //$conexion = & new PDO("mysql:dbname={$Base};port={$Puerto};host={$Servidor}", $Usuario, $Password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
      $conexion = new PDO("mysql:host=$Servidor;dbname=$Base", $Usuario, $Password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
    //echo 'hola';
  }
    catch (PDOException $e) {
        die(utf8_decode('Fallo la conexion MYSQL ').$e->getMessage());
    }
  


      
    $nombres = $_POST["nombres"];
    $apellidos = $_POST["apellidos"];
    $dni = $_POST["dni"];
    $correo = $_POST["correo"];
    $telefono = $_POST["telefono"];

    $id_tipo_matricula = $_POST["id_tipo_matricula"]; 

    $curso_principal = $_POST["curso_principal"];
    $curso_secundario = $_POST["curso_secundario"];
      

      $conexion->beginTransaction();//inicio de Transaccion

    //Validar Asistente
    $sql = "SELECT * FROM  asistente WHERE  dni = '$dni' ";        
    $result = $conexion->prepare($sql);
    $result->execute(array());

    $asistente = $result->fetch();
    $dni_find = $asistente['dni'];

    //die($dni_find.'-'.$dni);

    if ($dni_find == $dni) {
        $conexion->rollBack(); 
        $respuesta  = array( 
        'estado' => 0 ,
        'error' => $result->errorInfo(),
        'mensaje' => "Usuario ya se encuentra registrado, comuniquese con CTI." );
        die(json_encode($respuesta));
    }


    //INSERTAR ASISTENTE
    $sql = "INSERT INTO asistente  ( nombres, apellidos,  dni, correo, telefono) 
            VALUES ( '$nombres', '$apellidos', '$dni', '$correo', '$telefono') ";        
    $result = $conexion->prepare($sql);
    $result->execute(array());


    if ($result->errorCode() != '00000') {
        $conexion->rollBack(); 
        $respuesta  = array( 
        'estado' => 0 ,
        'error' => $result->errorInfo(),
        'mensaje' => "Error al Grabar Registro - Datos asistente" );
        die(json_encode($respuesta));
    }

    //INSERTAR MATRICULA
    $id_asistente = $conexion->lastInsertId();


    $sql = "SELECT * FROM tipo_matricula WHERE id_tipo_matricula = {$id_tipo_matricula} ";        
    $result = $conexion->prepare($sql);
    $result->execute(array());   
    $tipo_matricula = $result->fetch();
    $costo = $tipo_matricula['precio'];


    $sql = "INSERT INTO matricula  ( id_tipo_matricula, costo_matricula, fecha_registro, id_asistente, estado_matricula) 
            VALUES ( {$id_tipo_matricula}, {$costo},  now(), {$id_asistente}, 'prematricula' ) ";        
    $result = $conexion->prepare($sql);
    $result->execute(array());

    if ($result->errorCode() != '00000') {
        $conexion->rollBack(); 
        $respuesta  = array( 
        'estado' => 0 ,
        'error' => $result->errorInfo(),
        'mensaje' => "Error al Grabar Registro - Datos matricula" );
        die(json_encode($respuesta));
    }

    $id_matricula = $conexion->lastInsertId();

    //INSERTAR CURSO_MATRICULA 1
    /*$sql = "INSERT INTO curso_matricula  ( id_curso, id_matricula, prioridad, estado)  VALUES ( {$curso_principal}, {$id_matricula}, 1, 1 ) ";        
    $result = $conexion->prepare($sql);
    $result->execute(array());

    if ($result->errorCode() != '00000') {
        $conexion->rollBack(); 
        $respuesta  = array( 
        'estado' => 0 ,
        'error' => $result->errorInfo(),
        'mensaje' => "Error al Grabar Registro - Datos cursos y matricula " );
        die(json_encode($respuesta));
    }

    if($curso_secundario != ''){
    
        //INSERTAR CURSO_MATRICULA 2
        $sql = "INSERT INTO curso_matricula  ( id_curso, id_matricula, prioridad, estado) 
                VALUES ( $curso_secundario, $id_matricula, 2, 1 ) ";        
        $result = $conexion->prepare($sql);
        $result->execute(array());

        if ($result->errorCode() != '00000') {
            $conexion->rollBack(); 
            $respuesta  = array( 
            'estado' => 0 ,
            'error' => $result->errorInfo(),
            'mensaje' => "Error al Grabar Registro - Datos cursos y matricula  " );
            die(json_encode($respuesta));
        }

    }*/

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    //Load composer's autoloader
    require '../vendor/autoload.php';                     // Passing `true` enables exceptions

    //Create a new PHPMailer instance
    $mail = new PHPMailer();  
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'ctiunsm@gmail.com';                 // SMTP username
    $mail->Password = 'ctiunsm2019.';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;  

    $mail->setFrom('ctiunsm@gmail.com', 'CTI-UNSM');
    $mail->addAddress('$correo', 'New User'); 
    $mail->addCC('ctiunsm@gmail.com'); 

    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Nueva solicitud matricula';
    $mail->Body    .= "Nombres : <b>$nombres</b> <br>";
    $mail->Body    .= "Apellidos : <b>$apellidos</b> <br>";
    $mail->Body    .= "DNI : <b>$dni</b> <br>";
    $mail->Body    .= "Correo : <b>$correo</b> <br>";
    $mail->Body    .= "Telefono/Celular : <b>$telefono</b> <br>";
    $mail->Body    .= "Costo : <b>{$costo}</b> <br><br>";
    $mail->Body .= 'Para más informacion en Oficina en Jr. Orellana 575 - Tarapoto. Telefono 042 -480142 / 955 941 992. Correo ctiunsm@gmail.com.';

    $mail->AltBody  = "Nuevo usuario; <b>$nombres</b>; <b>$apellidos</b>; <b>$dni</b>; <b>$correo</b>; <b>$telefono</b>.";

    $mail->send();


    //CORRECTO
    $conexion->commit();
    $respuesta  = array( 
        'estado' => 1 ,
        'error' => '',
        'mensaje' => "Registro grabado correctamente" );
    die(json_encode($respuesta));



?>