
	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			
			<!-- Newsletter -->

			<!--div class="newsletter">
				<div class="row">
					<div class="col">
						<div class="section_title text-center">
							<h1>Subscribe to newsletter</h1>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col text-center">
						<div class="newsletter_form_container mx-auto">
							<form action="post">
								<div class="newsletter_form d-flex flex-md-row flex-column flex-xs-column align-items-center justify-content-center">
									<input id="newsletter_email" class="newsletter_email" type="email" placeholder="Email Address" required="required" data-error="Valid email is required.">
									<button id="newsletter_submit" type="submit" class="newsletter_submit_btn trans_300" value="Submit">Subscribe</button>
								</div>
							</form>
						</div>
					</div>
				</div>

			</div-->

			<!-- Footer Content -->

			<div class="footer_content">
				<div class="row">

					<!-- Footer Column - About -->
					<div class="col-lg-3 footer_col">

						<!-- Logo -->
						<div class="logo_container">
							<div class="logo">
								<img src="<?= $imagenes_web['logo_pie'] ?>" alt="">
							</div>
						</div>

						<p class="footer_about_text">Centro en Tecnologías de Información
						Somos tu llave para triunfar 
						No te quedes atrás y sé el mejor</p>

					</div>

					<!-- Footer Column - Menu -->

					<div class="col-lg-3 footer_col">
						<div class="footer_column_title">Menu</div>
						<div class="footer_column_content">
							<ul>

								<? foreach ($modulos_web as $key => $val) : ?>
									<li class='footer_list_item'><a href='<?= $val[1] ?>'><?= $val[0] ?></a></li>
								<? endforeach; ?>

							</ul>
						</div>
					</div>

					<!-- Footer Column - Usefull Links -->

					<div class="col-lg-3 footer_col">
						<div class="footer_column_title">Enlaces</div>
						<div class="footer_column_content">
							<ul>
								<li class="footer_list_item"><a href="https://unsm.edu.pe">Pagina web de UNSM</a></li>
								<li class="footer_list_item"><a href="http://cpu.unsm.edu.pe/">Pagina web de CPU - UNSM</a></li>
								<li class="footer_list_item"><a href="https://unsm.edu.pe/centros-de-produccion/idiomas/">Centro de Idiomas</a></li>
								<li class="footer_list_item"><a href="https://unsm.edu.pe/investigacion/">Investigación y desarrollo</a></li>
								<!-- <li class="footer_list_item"><a href="#">Tuitions</a></li> -->
							</ul>
						</div>
					</div>

					<!-- Footer Column - Contact -->

					<div class="col-lg-3 footer_col">
						<div class="footer_column_title">Contacto</div>
						<div class="footer_column_content">
							<ul>
								
								<? foreach ($informacion_contacto['direccion'] as $key => $value) : ?>
								<li class="footer_contact_item">
									<div class="footer_contact_icon">
										<img src="images/placeholder.svg" alt="https://www.flaticon.com/authors/lucy-g">
									</div>									
									<?= $value ?>
								</li>
								<? endforeach; ?>

								
								<li class="footer_contact_item">
									<div class="footer_contact_icon">
										<img src="images/smartphone.svg" alt="https://www.flaticon.com/authors/lucy-g">
									</div>	
									<? foreach ($informacion_contacto['telefono'] as $key => $value) : ?>								
									<?= '('.$value .') '?>

									<? endforeach; ?>
								</li>
								

								<? foreach ($informacion_contacto['correo'] as $key => $value) : ?>
								<li class="footer_contact_item">
									<div class="footer_contact_icon">
										<img src="images/envelope.svg" alt="https://www.flaticon.com/authors/lucy-g">
									</div>									
									<?= $value ?>
								</li>
								<? endforeach; ?>

							</ul>
						</div>
					</div>

				</div>
			</div>

			<!-- Footer Copyright -->

			<div class="footer_bar d-flex flex-column flex-sm-row align-items-center">
				<div class="footer_copyright">
					<span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by Trebloc and  <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
				</div>
				<div class="footer_social ml-sm-auto">
					<ul class="menu_social">
						<? foreach ($redes_sociales as $key => $val) : ?>
						 	<li class="menu_social_item"><a href="<?= $val['link'] ?>"><i class='<?= $val['icono'] ?>'></i></a></li>
						<? endforeach; ?>	
					</ul>
				</div>
			</div>

		</div>
	</footer>

</div>
<style type="text/css">
#clock {
  font-size: 5em;
  text-align: center;
  margin: 1em;
}
</style>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/scrollTo/jquery.scrollTo.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>

</body>
</html>