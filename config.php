<?php 


	/*$Servidor = "localhost";////"localhost";//"192.168.1.39";//149.56.134.245
	$Puerto   = "3306"; //"5432"; //"5434";
	$Usuario  = "root"; //"corp"; //"postgres"//admin_default;
	$Password = ""; //"@dmin$7391&"; //$CriptF;//admin6
	$Base     = "admin_cti"; //"corp_sicuani"; //"e-siincoweb_empssapal";*/

	$Servidor = "localhost";////"localhost";//"192.168.1.39";//149.56.134.245
 	$Puerto   = "3306"; //"5432"; //"5434";
	$Usuario  = "root"; //"corp"; //"postgres"//admin_default;
	$Password = ""; //"@dmin$7391&"; //$CriptF;//admin6
 	$Base     = "admin_cti"; //"corp_sicuani"; //"e-siincoweb_empssapal";


	try {
		$conexion = new PDO("mysql:dbname=$Base;port=$Puerto;host=$Servidor", $Usuario, $Password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
	}
	catch (PDOException $e) {
		die(utf8_decode('Fallo la conexion Postgres ').$e->getMessage());
	}


	//$sentencia = $conexion->query('select * from asistente');
	//print_r($sentencia->fetchAll());
	
	//Globales
	$etiqueta_pagina = 'CTI-UNSM' ;
	$nombre_evento = 'II Semininario Internacional de Tecnología' ;
	
	$informacion_contacto = array(
		'correo' =>  array( 'ctiunsm@gmail.com' ),
		'telefono' => array( '042 -480142',  '955 941 992' , '944 929 637' ),
		'telefono_principal' => '955 941 992',
		'fanpage' => array( 'www.facebook.com/cti.unsm'),
		'direccion' => array( 'Jr. Orellana 575 - Tarapoto'),
	);

	$redes_sociales  = array(  
	 	'facebook' => array('icono' => 'fab fa-facebook-f', 'link' => 'https://www.facebook.com/cti.unsm' ),
	 	'pinterest' => array('icono' => 'fab fa-pinterest', 'link' => 'https://www.pinterest.es/ctiunsm/' ),
	 	// 'linkedin-in' => array('icono' => 'fab fa-linkedin-in', 'link' => 'https://www.facebook.com/cti.unsm' ),
	 	'instagram' => array('icono' => 'fab fa-instagram', 'link' => 'https://www.instagram.com/cti_unsm/?hl=es-la' ),
	 	'twitter' => array('icono' => 'fab fa-twitter', 'link' => 'https://twitter.com/CtiUnsm' )
					   );

	$imagenes_web= array(
		'logo_cabecera' => 'images/cti_logo.png', 
		'logo_pie' => 'images/logo.png',
		'fondo' => array( 'inicio' => 'images/unsm.jpg' , 
						  'nosotros' => 'images/unsm-tarapoto-cpu-postulantes.jpg' , 
						  'contacto' => 'images/unsm-computo.jpg'  ),
	);

	$modulos_web  = array(  array('Inicio', 'index.php'),
						array('Nosotros', 'nosotros.php'),
						array('Talleres', 'talleres.php'),
						array('Ponentes', 'ponentes.php'),
						array('Aliados', 'Aliados.php'),
						array('Contacto', 'contacto.php')
					);
	// $sql = "SELECT * FROM cursos ";
	// $result = $conexion->prepare($sql);
	// $result->execute(array());   
	// $cursos = $result->fetchAll();
	// foreach ($cursos as $key => $val) {
		
	// }
											
	

	$cursos_web = array(  
		// array('img' => 'images/talleres/senasa/foto_1.jpg',
		// 			'card-title' => "Uso de sistemas de información geográfica utilizando ArcGis en gestión de Brotes Epidémicos",
		// 			'card-text' => "Dirigido a funcionarios, profesionales y técnicos de SENASA, en su Programa de Fortalecimiento de Capacidades.",
		// 			'course_author_image' => "",
		// 			'course_author_name' => "",
		// 			'course_price' => "" ) ,
		      array('img' => 'images/talleres/topografia_1.jpg',
					'card-title' => "MANEJO DE ESTACION TOTAL",
					'card-text' => "Manejo de la estación total modelo TOPCON ES-105, GPS topográfico, Levantamiento y replanteo topográfico de una carretera y batimetría.",
					'course_author_image' => "",
					'course_author_name' => "",
					'course_price' => "" ),

			  array('img' => 'images/talleres/seminario1.jpg',
					'card-title' => "II SEMIRARIO INTERNACIONAL ",
					'card-text' => " Dirigido a estudiantes, profesionales y público en general conocedores de herramientas tecnológicas, los ponenetes presentaran cursos talleres para complementar las idea abordadas en su ponencias   ",
					'course_author_image' => "",
					'course_author_name' => "",
					'course_price' => "" ) ,
					
			  array('img' => 'images/talleres/siaf_1.jpg',
					'card-title' => "CURSO TALLER DE SiAF ",
					'card-text' => "El Sistema de Administración Financiera Siaf es un software de uso obligatorio en Ejecución en forma correcta de los presupuestos asignados para todas las Entidades PÚBLICAS a nivel Local, Regional y Nacional.",
					'course_author_image' => "",
					'course_author_name' => "",
					'course_price' => " " ) 
	);
?>