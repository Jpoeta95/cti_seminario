const getRemainingTime = deadline => {
  let now = new Date(),
      remainTime = (new Date(deadline) - now + 1000) / 1000,
      remainSeconds = ('0' + Math.floor(remainTime % 60)).slice(-2),
      remainMinutes = ('0' + Math.floor(remainTime / 60 % 60)).slice(-2),
      remainHours = ('0' + Math.floor(remainTime / 3600 % 24)).slice(-2),
      remainDays = Math.floor(remainTime / (3600 * 24));

  return {
    remainSeconds,
    remainMinutes,
    remainHours,
    remainDays,
    remainTime
  }
};

const countdown = (deadline,elem,finalMessage) => {
  const el = document.getElementById(elem);
  const eld = document.getElementById('Countdown-dia');
  const elm = document.getElementById('Countdown-hora');
  const elh = document.getElementById('Countdown-minuto');

console.log('entro');
  const timerUpdate = setInterval( () => {
    let t = getRemainingTime(deadline);
    //el.innerHTML = `${t.remainDays}D:${t.remainHours}H:${t.remainMinutes}M:${t.remainSeconds}S`;
    eld.innerHTML = `${t.remainDays}`;
    elm.innerHTML = `${t.remainHours}`;
    elh.innerHTML = `${t.remainMinutes}`;


    if(t.remainTime <= 1) {
      clearInterval(timerUpdate);
      el.innerHTML = finalMessage;
    }

  }, 1000)
};

countdown('Oct 24 2019 00:00:00 GMT-0500', 'Countdown-content', '¡Ya empezó!');